﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Video;

public class CheckConsciousness : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private GameObject fillBar;
    [SerializeField] private VideoPlayer brainbeat;

    private bool isDown = false;
    
    void Update()
    {
        if (isDown)
        {          
            if (Input.GetMouseButton(0))
            {
                if (fillBar.GetComponent<Image>().fillAmount == 1)
                {
                    brainbeat.Stop();
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                    {
                        Feedback.checkedConscious1 = true;
                    }    
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                    {
                        Feedback.checkedConscious2 = true;                                           
                    }    
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                    {
                        Feedback.checkedConscious3 = true;                                           
                    }
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                    {
                        Feedback.checkedConscious4 = true;                                           
                    } 
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                    {
                        Feedback.checkedConscious5 = true;                                           
                    } 
                }
                else
                {
                    fillBar.GetComponent<Image>().fillAmount += 0.012f;
                }                
            }
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
        //this.GetComponent<AudioSource>().Play();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
        //this.GetComponent<AudioSource>().Play();
    }
}

