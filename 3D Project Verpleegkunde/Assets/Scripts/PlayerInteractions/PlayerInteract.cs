﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerInteract : MonoBehaviour
{
    [SerializeField] private GameObject inventory;
    [SerializeField] private float raycastDistance;
    [SerializeField] private InventoryObject inventoryObject;
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject reticle;
    [SerializeField] private GameObject patientInteractMenu;
    
    [SerializeField] private GameObject briefingMenu;
    [SerializeField] private GameObject interactItem;
    [SerializeField] private GameObject interactPatient;
    [SerializeField] private GameObject interactClipboard;
    [SerializeField] private GameObject interactButton;
    //[SerializeField] private GameObject interactBriefing;
    [SerializeField] private GameObject endScreen;
    [SerializeField] private GameObject feedback;
    [SerializeField] private GameObject mainUI;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject phaseScreen;

    //[SerializeField] private GameObject startCamera;
    [SerializeField] private Camera playerCamera;

    [SerializeField] private GameObject[] patients;
    public static int patientCount;
    [SerializeField] private GameObject patientInfoPrefab;
    [SerializeField] private GameObject clipboard;

    //[SerializeField] private GameObject briefing;
    //[SerializeField] private Camera briefingCamera;
    [SerializeField] private TMP_Text numberOfPatientsText;

    [SerializeField] private GameObject[] spawnpoints;
    [SerializeField] private List<GameObject> patientsList;

    [SerializeField] private GameObject patientHeadZoomMenu;
    [SerializeField] private GameObject patientHandZoomMenu;
    [SerializeField] private GameObject patientLegZoomMenu;

    [SerializeField] private GameObject[] patientInjuryUI;
    [SerializeField] private GameObject[] patientInteractUIToHide;
    [SerializeField] private GameObject[] patient1InteractUIToHide;
    [SerializeField] private GameObject[] patient2InteractUIToHide;
    [SerializeField] private GameObject[] patient3InteractUIToHide;
    [SerializeField] private GameObject[] patient4InteractUIToHide;
    [SerializeField] private GameObject[] patient5InteractUIToHide;
    [SerializeField] private GameObject[] legButtons;
    [SerializeField] private GameObject patient1Interact;
    [SerializeField] private GameObject patient2Interact;
    [SerializeField] private GameObject patient3Interact;
    [SerializeField] private GameObject patient4Interact;
    [SerializeField] private GameObject patient5Interact;

    [SerializeField] private int minimumPatients = 0;
    [SerializeField] private int maximumPatients = 0;
    [SerializeField] private TMP_Text ScoreText;
    [SerializeField] private GameObject[] phases;

    [SerializeField] private GameObject headPatient1;
    [SerializeField] private GameObject headPatient2;
    [SerializeField] private GameObject headPatient3;
    [SerializeField] private GameObject headPatient4;
    [SerializeField] private GameObject headPatient5;

    [SerializeField] private GameObject handPatient1;
    [SerializeField] private GameObject handPatient2;
    [SerializeField] private GameObject handPatient3;
    [SerializeField] private GameObject handPatient4;
    [SerializeField] private GameObject handPatient5;

    [SerializeField] private GameObject legPatient2;
    [SerializeField] private GameObject legPatient3;

    [SerializeField] private GameObject legPatient2Screen;
    [SerializeField] private GameObject legPatient3Screen;

    public static int Score = 0;

    private RaycastHit hit;
    [HideInInspector] public GameObject temporaryCamera;
    private GameObject temporaryHeadCamera;
    private GameObject temporaryHandCamera;
    private GameObject temporaryLegCamera;
    private GameObject temporaryGameObject;
    public static GameObject[] patientInfoPrefabArray;
    
    private GameObject[] patientCameras;

    private int patientsToSpawn;
    private int indexOfPrefab;
    private GameObject temporaryPatient;
    private bool pickedUpBackpack = false;
    private bool continueButtonPressed = false;
    public static bool startTreatment = false;

    [HideInInspector] public static Patient patient;
    [HideInInspector] public PatientInteraction patientInteraction;

    public static List<int> patientTreatmentOrder;
    private bool alreadyInArray1 = false;
    private bool alreadyInArray2 = false;
    private bool alreadyInArray3 = false;
    private bool alreadyInArray4 = false;
    private bool alreadyInArray5 = false;

    private bool setLegButtonStatus = true;

    void Awake()
    {
        playerCamera.gameObject.SetActive(true);

        patientTreatmentOrder = new List<int>();

        //calculate random number of patients
        patientsToSpawn = Random.Range(minimumPatients, spawnpoints.Length + 1);

        //set briefing text
        numberOfPatientsText.text = patientsToSpawn.ToString();

        //instantiate generated number of patient prefabs on spawn points
        for (int i = 0; i < patientsToSpawn; i++)
        {
            indexOfPrefab = Random.Range(0, patientsList.Count);
            temporaryPatient = Instantiate(patientsList[indexOfPrefab], spawnpoints[i].transform.position, spawnpoints[i].transform.localRotation);
            temporaryPatient.transform.SetParent(spawnpoints[i].transform, true);
            temporaryPatient.name = "Patient " + (i + 1);
            //set right injurie drops active with patient
            patientsList.RemoveAt(indexOfPrefab);
        }

        foreach (InventorySlot item in inventoryObject.Container)
        {
            item.amount = 0;
        }

        continueButtonPressed = false;
        startTreatment = false;

        alreadyInArray1 = false;
        alreadyInArray2 = false;
        alreadyInArray3 = false;
        alreadyInArray4 = false;
        alreadyInArray5 = false;

        setLegButtonStatus = true;
    }

    void Start()
    {
        patients = GameObject.FindGameObjectsWithTag("Patient");
        patientInfoPrefabArray = new GameObject[patients.Length];
        patientCount = patients.Length;

        for (int i = 0; i < patients.Length; i++)
        {
            temporaryGameObject = Instantiate(patientInfoPrefab);
            temporaryGameObject.name = "Patient Info " + (i + 1);
            temporaryGameObject.transform.SetParent(clipboard.transform, false);
            patientInfoPrefabArray[i] = temporaryGameObject;
        }

        foreach (GameObject item in patientInfoPrefabArray)
        {
            item.SetActive(false);
        }

        patientInteraction = new PatientInteraction();
        //playerCamera.GetComponent<AudioListener>().enabled = false;
        reticle.SetActive(false);
    }

    void Update()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, raycastDistance))
        {
            if (hit.collider.gameObject.tag == "PickUp")
            {
                interactItem.SetActive(true);
            }  
            if (hit.collider.gameObject.tag == "Patient")
            {
                interactPatient.SetActive(true);
            }
            if (hit.collider.gameObject.tag == "Button")
            {
                interactButton.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.collider.gameObject.tag == "PickUp")
                {
                    pickedUpBackpack = true;
                    Destroy(hit.collider.gameObject);
                }

                if (hit.collider.gameObject.tag == "Patient")
                {
                    reticle.SetActive(false);
                    patientInteractMenu.SetActive(true);
                    /*
                    foreach (GameObject item in phases)
                    {
                        item.SetActive(false);
                    }
                    */
                    patient = hit.collider.gameObject.GetComponent<Patient>();

                    temporaryCamera = hit.collider.gameObject.transform.GetChild(0).gameObject;
                    MoveCamera(temporaryCamera);

                    for (int i = 0; i < patients.Length + 1; i++)
                    {
                        if (hit.collider.gameObject.name == "Patient " + (i + 1))
                        {
                            patientInfoPrefabArray[i].SetActive(true);
                            //set this patient injury UI on

                            if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                            {
                                //set patient 1 injury UI active
                                //patientInteractMenu.SetActive(true);
                                patient1Interact.SetActive(true);
                                GameTimer.selectedPatient1.SetActive(true);
                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 1 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }

                                if (GameFases.examinedAllPatients && !alreadyInArray1)
                                {
                                    patientTreatmentOrder.Add(hit.collider.gameObject.GetComponent<Patient>().patientInfo.urgency);
                                    alreadyInArray1 = true;
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                            {
                                //set patient 2 injury UI active
                                //patientInteractMenu.SetActive(true);
                                patient2Interact.SetActive(true);
                                GameTimer.selectedPatient2.SetActive(true);
                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 2 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }

                                if (GameFases.examinedAllPatients && !alreadyInArray2)
                                {
                                    patientTreatmentOrder.Add(hit.collider.gameObject.GetComponent<Patient>().patientInfo.urgency);
                                    alreadyInArray2 = true;
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                            {
                                //set patient 3 injury UI active
                                //patient3InteractMenu.SetActive(true);
                                patient3Interact.SetActive(true);
                                GameTimer.selectedPatient3.SetActive(true);
                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 3 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }

                                if (GameFases.examinedAllPatients && !alreadyInArray3)
                                {
                                    patientTreatmentOrder.Add(hit.collider.gameObject.GetComponent<Patient>().patientInfo.urgency);
                                    alreadyInArray3 = true;
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                            {
                                //set patient 4 injury UI active
                                //patient4InteractMenu.SetActive(true);
                                patient4Interact.SetActive(true);
                                GameTimer.selectedPatient4.SetActive(true);
                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 4 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }

                                if (GameFases.examinedAllPatients && !alreadyInArray4)
                                {
                                    patientTreatmentOrder.Add(hit.collider.gameObject.GetComponent<Patient>().patientInfo.urgency);
                                    alreadyInArray4 = true;
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                            {
                                //set patient 5 injury UI active
                                //patient5InteractMenu.SetActive(true);
                                patient5Interact.SetActive(true);
                                GameTimer.selectedPatient5.SetActive(true);
                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 5 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }

                                if (GameFases.examinedAllPatients && !alreadyInArray5)
                                {
                                    patientTreatmentOrder.Add(hit.collider.gameObject.GetComponent<Patient>().patientInfo.urgency);
                                    alreadyInArray5 = true;
                                }
                            }
                        }
                    }
                }

                if (hit.collider.gameObject.tag == "Button")
                {
                    ScoreCalculator.Calculator();
                    feedback.SetActive(true);
                    ScoreText.text = Score.ToString();
                }                
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (headPatient1.activeInHierarchy || headPatient2.activeInHierarchy || headPatient3.activeInHierarchy || headPatient4.activeInHierarchy || headPatient5.activeInHierarchy)
            {
                Debug.Log("escapeD");
                MoveZoomCameraHeadBack();
                headPatient1.SetActive(false);
                headPatient2.SetActive(false);
                headPatient3.SetActive(false);
                headPatient4.SetActive(false);
                headPatient5.SetActive(false);
            }
            else if (handPatient1.activeInHierarchy || handPatient2.activeInHierarchy || handPatient3.activeInHierarchy || handPatient4.activeInHierarchy || handPatient5.activeInHierarchy)
            {
                Debug.Log("escapeD");
                MoveZoomCameraHandBack();
                handPatient1.SetActive(false);
                handPatient2.SetActive(false);
                handPatient3.SetActive(false);
                handPatient4.SetActive(false);
                handPatient5.SetActive(false);
            }
            else if (legPatient2.activeInHierarchy || legPatient3.activeInHierarchy)
            {
                MoveZoomCameraLegBack();
                legPatient2.SetActive(false);
                legPatient3.SetActive(false);
            }
            else
            {
                if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                {
                    Patient1MenuBackButton();
                }
                else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                {
                    Patient2MenuBackButton();
                }
                else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                {
                    Patient3MenuBackButton();
                }
                else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                {
                    Patient4MenuBackButton();
                }
                else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                {
                    Patient5MenuBackButton();
                }
            }
        }

        if (pickedUpBackpack && GameFases.examinedAllPatients)
        {
            inventory.SetActive(true);
        }

        if (GameFases.examinedAllPatients && !continueButtonPressed)
        {
            phaseScreen.SetActive(true);
            PhaseScreen();
            continueButtonPressed = true;
        }

        if (!Physics.Raycast(transform.position, transform.forward, out hit, raycastDistance) || hit.collider.gameObject.tag == "Wall")
        {
            //To prevent it from detecting objects
            interactItem.SetActive(false);
            interactPatient.SetActive(false);
            interactButton.SetActive(false);
            //interactBriefing.SetActive(false);
        }

        if (patientInteractMenu.activeInHierarchy || briefingMenu.activeInHierarchy || endScreen.activeInHierarchy || pauseMenu.activeInHierarchy || feedback.activeInHierarchy||startMenu.activeInHierarchy||phaseScreen.activeInHierarchy)
        {
            interactItem.SetActive(false);
            interactPatient.SetActive(false);
            interactButton.SetActive(false);
            //interactBriefing.SetActive(false);
            //mainUI.SetActive(false);
            this.gameObject.GetComponent<FirstPersonController>().enabled = false;
            this.gameObject.GetComponent<PauseMenu>().enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            //mainUI.SetActive(true);
            this.gameObject.GetComponent<FirstPersonController>().enabled = true;
            this.gameObject.GetComponent<PauseMenu>().enabled = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (GameFases.examinedAllPatients && (!legPatient2Screen.activeInHierarchy || !legPatient3Screen.activeInHierarchy) && setLegButtonStatus)
        {
            Debug.Log("leg button on");
            foreach (GameObject item in legButtons)
            {
                item.SetActive(true);
            }
        }
    }

    private void OnApplicationQuit()
    {
        foreach (InventorySlot item in inventoryObject.Container)
        {
            item.amount = 0;
        }
    }

    public void Patient1MenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        patientInteractMenu.SetActive(false);
        //inventory false
        clipboard.SetActive(false);
        inventory.SetActive(false);
        reticle.SetActive(true);
        /*
        foreach (GameObject item in phases)
        {
            item.SetActive(true);
        }
        */

        foreach (GameObject item in patientInfoPrefabArray)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }

        patient1Interact.SetActive(false);
        GameTimer.selectedPatient1.SetActive(false);            
    }
    public void Patient2MenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        patientInteractMenu.SetActive(false);
        //inventory false
        clipboard.SetActive(false);
        inventory.SetActive(false);
        reticle.SetActive(true);
        /*
        foreach (GameObject item in phases)
        {
            item.SetActive(true);
        }
        */

        foreach (GameObject item in patientInfoPrefabArray)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }
        

        patient2Interact.SetActive(false);
        GameTimer.selectedPatient2.SetActive(false);        
    }
    public void Patient3MenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        patientInteractMenu.SetActive(false);
        //inventory false
        inventory.SetActive(false);
        reticle.SetActive(true);
        /*
        foreach (GameObject item in phases)
        {
            item.SetActive(true);
        }
        */
        foreach (GameObject item in patientInfoPrefabArray)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }
        patient3Interact.SetActive(false);
        GameTimer.selectedPatient3.SetActive(false);     
    }
    public void Patient4MenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        patientInteractMenu.SetActive(false);
        //inventory false
        inventory.SetActive(false);
        reticle.SetActive(true);
        /*
        foreach (GameObject item in phases)
        {
            item.SetActive(true);
        }
        */

        foreach (GameObject item in patientInfoPrefabArray)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }
        patient4Interact.SetActive(false);
        GameTimer.selectedPatient4.SetActive(false);     
    }
    public void Patient5MenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        patientInteractMenu.SetActive(false);
        //inventory false
        inventory.SetActive(false);
        reticle.SetActive(true);
        /*
        foreach (GameObject item in phases)
        {
            item.SetActive(true);
        }
        */

        foreach (GameObject item in patientInfoPrefabArray)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }
        patient5Interact.SetActive(false);
        GameTimer.selectedPatient5.SetActive(false);     
    }



    public void BriefingStart()
    {
        //set first person camera active
        briefingMenu.SetActive(false);
        playerCamera.GetComponent<AudioListener>().enabled = true;
        //set retilce active
        reticle.SetActive(true);
    }

    public void BriefingBackButton()
    {
        MoveBriefingCameraBack(temporaryCamera);
        briefingMenu.SetActive(false);
        reticle.SetActive(true);
    }

    private void MoveCameraToBriefing(GameObject cameraObject)
    {
        playerCamera.gameObject.SetActive(false);
        cameraObject.gameObject.SetActive(true);
    }

    private void MoveBriefingCameraBack(GameObject cameraObject)
    {
        cameraObject.gameObject.SetActive(false);
        playerCamera.gameObject.SetActive(true);
    }
    public void MoveHeadCamera()
    {
        temporaryHeadCamera = hit.collider.gameObject.transform.GetChild(1).gameObject;
        temporaryHeadCamera.gameObject.SetActive(true);
        playerCamera.gameObject.SetActive(false);
        //set patient interact off except for clipboard
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in legButtons)
        {
            item.SetActive(false);
        }
        setLegButtonStatus = false;
    }

    public void MoveHandCamera()
    {
        temporaryHandCamera = hit.collider.gameObject.transform.GetChild(2).gameObject;
        temporaryHandCamera.gameObject.SetActive(true);
        playerCamera.gameObject.SetActive(false);
        //set patient interact off except for clipboard
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in legButtons)
        {
            item.SetActive(false);
        }
        setLegButtonStatus = false;
    }
    public void MoveLegCamera()
    {
        temporaryLegCamera = hit.collider.gameObject.transform.GetChild(3).gameObject;
        temporaryLegCamera.gameObject.SetActive(true);
        playerCamera.gameObject.SetActive(false);
        //set patient interact off except for clipboard
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(false);
        }

        foreach (GameObject item in legButtons)
        {
            item.SetActive(false);
        }
        setLegButtonStatus = false;
    }

    public void MoveZoomCameraHandBack()
    {
        MoveCamera(temporaryCamera);
        temporaryHandCamera.gameObject.SetActive(false);
        //set patient interact on
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(true);
        }
        if (GameFases.examinedAllPatients)
        {
            foreach (GameObject item in legButtons)
            {
                item.SetActive(true);
            }
            setLegButtonStatus = true;
        }
    }

    public void MoveZoomCameraHeadBack()
    {
        MoveCamera(temporaryCamera);
        temporaryHeadCamera.gameObject.SetActive(false);
        //set patient interact on
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(true);
        }
        if (GameFases.examinedAllPatients)
        {
            foreach (GameObject item in legButtons)
            {
                item.SetActive(true);
            }
            setLegButtonStatus = true;
        }
    }

    public void MoveZoomCameraLegBack()
    {
        MoveCamera(temporaryCamera);
        temporaryLegCamera.gameObject.SetActive(false);
        //set patient interact on
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(true);
        }

        foreach (GameObject item in legButtons)
        {
            item.SetActive(true);
        }
        setLegButtonStatus = true;
    }

    public void MoveCamera(GameObject cameraObject)
    {
        playerCamera.gameObject.SetActive(false);
        cameraObject.gameObject.SetActive(true);
    }

    public void MoveCameraBack(GameObject cameraObject)
    {
        cameraObject.gameObject.SetActive(false);
        playerCamera.gameObject.SetActive(true);
    }
    public void PhaseScreen()
    {
        foreach (GameObject item in legButtons)
        {
            item.SetActive(true);
        }
        setLegButtonStatus = true;

        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            Patient1MenuBackButton();    
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            Patient2MenuBackButton();    
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            Patient3MenuBackButton();    
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            Patient4MenuBackButton();    
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            Patient5MenuBackButton();    
        }        
    }

    public void ContinueButton()
    {
        startTreatment = true;
    }

    public void EndGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
