﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Video;

public class CheckPulse : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private GameObject fillBar;
    [SerializeField] private VideoPlayer heartbeat;

    private bool isDown = false;
    
    void Update()
    {
        if (isDown)
        {            
            if (Input.GetMouseButton(0))
            {
                if (fillBar.GetComponent<Image>().fillAmount == 1)
                {
                    heartbeat.Stop();
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                    {
                        Feedback.checkedPulse1 = true;                                           
                    }    
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                    {
                        Feedback.checkedPulse2 = true;                                           
                    }    
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                    {
                        Feedback.checkedPulse3 = true;                                           
                    }
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                    {
                        Feedback.checkedPulse4 = true;                                           
                    } 
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                    {
                        Feedback.checkedPulse5 = true;                                           
                    } 
                }
                else
                {
                    fillBar.GetComponent<Image>().fillAmount += 0.01f;
                }                
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
        this.GetComponent<AudioSource>().Play();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
        this.GetComponent<AudioSource>().Stop();
    }
}
