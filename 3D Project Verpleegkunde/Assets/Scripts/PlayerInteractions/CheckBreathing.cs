﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Video;

public class CheckBreathing : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private GameObject fillBar;
    [SerializeField] private VideoPlayer lungbeat;
    private bool isDown = false;
    
    void Update()
    {
        if (isDown)
        {          
            if (Input.GetMouseButton(0))
            {
                if (fillBar.GetComponent<Image>().fillAmount == 1)
                {
                    lungbeat.Stop();
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                    {
                        Feedback.checkedBreathing1 = true;                                           
                    }    
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                    {
                        Feedback.checkedBreathing2 = true;                                           
                    }    
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                    {
                        Feedback.checkedBreathing3 = true;                                           
                    }
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                    {
                        Feedback.checkedBreathing4 = true;                                           
                    } 
                    if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                    {
                        Feedback.checkedBreathing5 = true;                                           
                    } 
                }
                else
                {
                    fillBar.GetComponent<Image>().fillAmount += 0.008f;
                }                
            }
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
        this.GetComponent<AudioSource>().Play();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
        this.GetComponent<AudioSource>().Stop();
    }
}
