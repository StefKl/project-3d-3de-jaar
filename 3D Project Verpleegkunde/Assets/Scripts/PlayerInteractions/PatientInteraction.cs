﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PatientInteraction : MonoBehaviour
{
    [SerializeField] private GameObject patientInteractionMenu;
    [SerializeField] private GameObject radialMenu;
    [SerializeField] private GameObject[] patientInteractionMenuItemsToHide;

    [SerializeField] private GameObject patient1Interact;
    [SerializeField] private GameObject patient2Interact;
    [SerializeField] private GameObject patient3Interact;
    [SerializeField] private GameObject patient4Interact;
    [SerializeField] private GameObject patient5Interact;

    [SerializeField] private Material orange;

    public static bool hasClickedOnButton = false;

    public void TriageButton()
    {
        foreach (GameObject item in patientInteractionMenuItemsToHide)
        {
            item.SetActive(false);
        }

        hasClickedOnButton = false;

        radialMenu.SetActive(true);
    }

    void Update()
    {
        if (hasClickedOnButton)
        {
            TriageBackbutton();
        }
    }

    void TriageBackbutton()
    {
        radialMenu.SetActive(false);
        foreach (GameObject item in patientInteractionMenuItemsToHide)
        {
            item.SetActive(true);
        }
    }
}

