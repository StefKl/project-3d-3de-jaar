﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCalculator : MonoBehaviour
{
    public static void Calculator()
    {
        //Patient 1
        if(Feedback.checkedConscious1)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedBreathing1)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedPulse1)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.hasBandagePatient1)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.hasOxygenPatient1)
        {
            PlayerInteract.Score++;
        }
        if (Feedback.pickCorrectColor1)
        {
            PlayerInteract.Score += 3;
        }

        //Patient 2
        if(Feedback.checkedConscious2)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedBreathing2)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedPulse2)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.hasBandagePatient2)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.pickCorrectColor2)
        {
            PlayerInteract.Score += 3;
        }

        //Patient 3
        if(Feedback.checkedConscious3)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedBreathing3)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedPulse3)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.hasSplintPatient3)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.pickCorrectColor3)
        {
            PlayerInteract.Score += 3;
        }

        //Patient 4
        if(Feedback.checkedConscious4)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedBreathing4)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedPulse4)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.hasBandagePatient4)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.pickCorrectColor4)
        {
            PlayerInteract.Score += 3;
        }

        //Patient 5
        if(Feedback.checkedConscious5)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedBreathing5)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.checkedPulse5)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.hasBandagePatient5)
        {
            PlayerInteract.Score++;    
        }
        if (Feedback.pickCorrectColor5)
        {
            PlayerInteract.Score += 3;
        }
    }   
}
