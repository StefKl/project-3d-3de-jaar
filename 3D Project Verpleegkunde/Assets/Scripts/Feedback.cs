﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Feedback : MonoBehaviour
{
    [SerializeField] private GameObject feedback;
    [SerializeField] private GameObject globalFeedback;
    [SerializeField] private GameObject patientFeedback;
    [SerializeField] private Sprite patient1;
    [SerializeField] private Sprite patient2;
    [SerializeField] private Sprite patient3;
    [SerializeField] private Sprite patient4;
    [SerializeField] private Sprite patient5;
    //[SerializeField] private List<Image> imagesToFill;
    [SerializeField] private List<GameObject> panelsToLink;
    [SerializeField] private List<GameObject> nextButtons;
    [SerializeField] private List<GameObject> backButtons;

    [SerializeField] private GameObject patient1Text;
    [SerializeField] private GameObject patient2Text;
    [SerializeField] private GameObject patient3Text;
    [SerializeField] private GameObject patient4Text;
    [SerializeField] private GameObject patient5Text;

    [SerializeField] private Image pickedColor1Image;
    [SerializeField] private Image pickedColor2Image;
    [SerializeField] private Image pickedColor3Image;
    [SerializeField] private Image pickedColor4Image;
    [SerializeField] private Image pickedColor5Image;

    [SerializeField] private Image triageColor1;
    [SerializeField] private Image triageColor2;
    [SerializeField] private Image triageColor3;
    [SerializeField] private Image triageColor4;
    [SerializeField] private Image triageColor5;

    [SerializeField] private Sprite checkmark;
    [SerializeField] private Sprite cross;

    [SerializeField] private Vector2[] textPositions;
    [SerializeField] private Vector2[] checkPositions;

    [SerializeField] private Material red;
    [SerializeField] private Material black;
    [SerializeField] private Material blue;
    [SerializeField] private Material green;
    [SerializeField] private Material yellow;
    [SerializeField] private Material orange;

    [SerializeField] private Image finalCheck1;
    [SerializeField] private Image finalCheck2;
    [SerializeField] private Image finalCheck3;
    [SerializeField] private Image finalCheck4;
    [SerializeField] private Image finalCheck5;

    [SerializeField] private Image GreenBorder;
    [SerializeField] private Image RedBorder;

    [SerializeField] private TMP_Text ScoreText;

    [SerializeField] private TMP_Text correctTriage;
    [SerializeField] private TMP_Text toTriage;
    [SerializeField] private Image treatedmark;

    [SerializeField] private TMP_Text checkFeedbackText;
    [SerializeField] private TMP_Text totalChecks;

    private bool needsTriagePoint1 = true;
    private bool needsTriagePoint2 = true;
    private bool needsTriagePoint3 = true;
    private bool needsTriagePoint4 = true;
    private bool needsTriagePoint5 = true;

    public static int Score = 0;

    #region patientInteractChecks
    public static bool hasBandagePatient1 = false;
    public static bool hasBandagePatient2 = false;
    public static bool hasSplintPatient3 = false;
    public static bool hasBandagePatient4 = false;
    public static bool hasBandagePatient5 = false;

    public static bool hasOxygenPatient1 = false;

    public static bool checkedConscious1 = false;
    public static bool checkedPulse1 = false;
    public static bool checkedBreathing1 = false;

    public static bool checkedConscious2 = false;
    public static bool checkedPulse2 = false;
    public static bool checkedBreathing2 = false;

    public static bool checkedConscious3 = false;
    public static bool checkedPulse3 = false;
    public static bool checkedBreathing3 = false;

    public static bool checkedConscious4 = false;
    public static bool checkedPulse4 = false;
    public static bool checkedBreathing4 = false;

    public static bool checkedConscious5 = false;
    public static bool checkedPulse5 = false;
    public static bool checkedBreathing5 = false;

    public static bool hasPickAColor1 = false;
    public static bool hasPickAColor2 = false;
    public static bool hasPickAColor3 = false;
    public static bool hasPickAColor4 = false;
    public static bool hasPickAColor5 = false;

    public static bool pickCorrectColor1 = false;
    public static bool pickCorrectColor2 = false;
    public static bool pickCorrectColor3 = false;
    public static bool pickCorrectColor4 = false;
    public static bool pickCorrectColor5 = false;

    public static Color pickedColor1;
    public static Color pickedColor2;
    public static Color pickedColor3;
    public static Color pickedColor4;
    public static Color pickedColor5;
    #endregion

    private GameObject[] patients;
    private Color zeroAlpha = new Color(0f, 0f, 0f, 0f);
    private Color oneAlpha = new Color(255f, 255f, 255f, 1f);
    private int triagedCorrectCount;

    private bool treatedInCorrectOrder = false;

    private bool patient1ChecksOrder = false;
    private bool patient2ChecksOrder = false;
    private bool patient3ChecksOrder = false;
    private bool patient4ChecksOrder = false;
    private bool patient5ChecksOrder = false;
    private int checkCounter;

    private bool patient1Counted = true;
    private bool patient2Counted = true;
    private bool patient3Counted = true;
    private bool patient4Counted = true;
    private bool patient5Counted = true;

    void Awake()
    {
        patients = GameObject.FindGameObjectsWithTag("Patient");
        totalChecks.text = patients.Length.ToString();
        hasBandagePatient1 = false;
        hasBandagePatient2 = false;
        hasSplintPatient3 = false;
        hasBandagePatient4 = false;
        hasBandagePatient5 = false;

        hasOxygenPatient1 = false;

        checkedConscious1 = false;
        checkedPulse1 = false;
        checkedBreathing1 = false;

        checkedConscious2 = false;
        checkedPulse2 = false;
        checkedBreathing2 = false;

        checkedConscious3 = false;
        checkedPulse3 = false;
        checkedBreathing3 = false;

        checkedConscious4 = false;
        checkedPulse4 = false;
        checkedBreathing4 = false;

        checkedConscious5 = false;
        checkedPulse5 = false;
        checkedBreathing5 = false;

        hasPickAColor1 = false;
        hasPickAColor2 = false;
        hasPickAColor3 = false;
        hasPickAColor4 = false;
        hasPickAColor5 = false;

        GreenBorder.enabled = false;
        RedBorder.enabled = true;

        needsTriagePoint1 = true;
        needsTriagePoint2 = true;
        needsTriagePoint3 = true;
        needsTriagePoint4 = true;
        needsTriagePoint5 = true;
        toTriage.text = patients.Length.ToString();

        treatedInCorrectOrder = false;
        treatedmark.sprite = cross;

        patient1ChecksOrder = false;
        patient2ChecksOrder = false;
        patient3ChecksOrder = false;
        patient4ChecksOrder = false;
        patient5ChecksOrder = false;
        checkCounter = 0;

        patient1Counted = true;
        patient2Counted = true;
        patient3Counted = true;
        patient4Counted = true;
        patient5Counted = true;
    }

    void Start()
    {
        for (int i = 0; i < patients.Length; i++)
        {
            if (patients[i].GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
            {
                patient1Text.transform.SetParent(panelsToLink[i].transform);

                if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                {
                    triageColor1.color = black.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                {
                    triageColor1.color = blue.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                {
                    triageColor1.color = green.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                {
                    triageColor1.color = orange.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                {
                    triageColor1.color = red.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                {
                    triageColor1.color = yellow.color;
                }

                patient1Text.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
            {
                patient2Text.transform.SetParent(panelsToLink[i].transform);

                if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                {
                    triageColor2.color = black.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                {
                    triageColor2.color = blue.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                {
                    triageColor2.color = green.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                {
                    triageColor2.color = orange.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                {
                    triageColor2.color = red.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                {
                    triageColor2.color = yellow.color;
                }

                patient2Text.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
            {
                patient3Text.transform.SetParent(panelsToLink[i].transform);

                if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                {
                    triageColor3.color = black.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                {
                    triageColor3.color = blue.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                {
                    triageColor3.color = green.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                {
                    triageColor3.color = orange.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                {
                    triageColor3.color = red.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                {
                    triageColor3.color = yellow.color;
                }

                patient3Text.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
            {
                patient4Text.transform.SetParent(panelsToLink[i].transform);

                if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                {
                    triageColor4.color = black.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                {
                    triageColor4.color = blue.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                {
                    triageColor4.color = green.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                {
                    triageColor4.color = orange.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                {
                    triageColor4.color = red.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                {
                    triageColor4.color = yellow.color;
                }

                patient4Text.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
            {
                patient5Text.transform.SetParent(panelsToLink[i].transform);

                if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                {
                    triageColor5.color = black.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                {
                    triageColor5.color = blue.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                {
                    triageColor5.color = green.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                {
                    triageColor5.color = orange.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                {
                    triageColor5.color = red.color;
                }
                else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                {
                    triageColor5.color = yellow.color;
                }

                patient5Text.SetActive(true);
            }
        }
    }

    void Update()
    {
        for (int i = 0; i < patients.Length; i++)
        {
            if (patients[i].GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
            {
                if (checkedConscious1)
                {
                    patient1Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing1)
                {
                    patient1Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse1)
                {
                    patient1Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient1)
                {
                    patient1Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasOxygenPatient1)
                {
                    patient1Text.transform.Find("OxygenMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    //set cross in feedback
                }

                if (hasPickAColor1)
                {
                    pickedColor1Image.color = pickedColor1;
                    pickedColor1Image.sprite = null;
                    if (pickedColor1 == triageColor1.color)
                    {
                        pickCorrectColor1 = true;
                    }
                }

                if (patient1Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient1Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient1Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient1Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (patient1Text.transform.Find("OxygenMark").gameObject.GetComponent<Image>().sprite == checkmark)
                                {
                                    if (hasPickAColor1)
                                    {
                                        if (pickedColor1 == triageColor1.color)
                                        {   
                                            GreenBorder.enabled = true;
                                            RedBorder.enabled = false;
                                            finalCheck1.sprite = checkmark;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
            {
                if (checkedConscious2)
                {
                    patient2Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing2)
                {
                    patient2Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse2)
                {
                    patient2Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient2)
                {
                    patient2Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor2)
                {
                    pickedColor2Image.color = pickedColor2;
                    pickedColor2Image.sprite = null;
                    if (pickedColor2 == triageColor2.color)
                    {
                        pickCorrectColor2 = true;
                    }
                }

                if (patient2Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient2Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient2Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient2Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor2)
                                {
                                    if (pickedColor2 == triageColor2.color)
                                    {
                                        GreenBorder.enabled = true;
                                        RedBorder.enabled = false;
                                        finalCheck2.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
            {
                if (checkedConscious3)
                {
                    patient3Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing3)
                {
                    patient3Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse3)
                {
                    patient3Text.transform.Find("PulseMark").gameObject.SetActive(false);
                    patient3Text.transform.Find("PulseCheckmark").gameObject.SetActive(true);
                }

                if (hasSplintPatient3)
                {
                    patient3Text.transform.Find("SplintMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor3)
                {
                    pickedColor3Image.color = pickedColor3;
                    pickedColor3Image.sprite = null;
                    if (pickedColor3 == triageColor3.color)
                    {
                        pickCorrectColor3 = true;
                    }
                }

                if (patient3Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient3Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient3Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient3Text.transform.Find("SplintMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor3)
                                {
                                    if (pickedColor3 == triageColor3.color)
                                    {
                                        GreenBorder.enabled = true;
                                        RedBorder.enabled = false;
                                        finalCheck3.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
            {
                if (checkedConscious4)
                {
                    patient4Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing4)
                {
                    patient4Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse4)
                {
                    patient4Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient4)
                {
                    patient4Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor4)
                {
                    pickedColor4Image.color = pickedColor4;
                    pickedColor4Image.sprite = null;
                    if (pickedColor4 == triageColor4.color)
                    {
                        pickCorrectColor4 = true;
                    }
                }

                if (patient4Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient4Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient4Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient4Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor4)
                                {
                                    if (pickedColor4 == triageColor4.color)
                                    {
                                        GreenBorder.enabled = true;
                                        RedBorder.enabled = false;
                                        finalCheck4.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
            {
                if (checkedConscious5)
                {
                    patient5Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing5)
                {
                    patient5Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse5)
                {
                    patient5Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient5)
                {
                    patient5Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor5)
                {
                    pickedColor5Image.color = pickedColor5;
                    pickedColor5Image.sprite = null;
                    if (pickedColor5 == triageColor5.color)
                    {
                        pickCorrectColor5 = true;
                    }
                }

                if (patient5Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient5Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient5Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient5Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor5)
                                {
                                    if (pickedColor5 == triageColor5.color)
                                    {
                                        GreenBorder.enabled = true;
                                        RedBorder.enabled = false;
                                        finalCheck5.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        if (pickCorrectColor1 && needsTriagePoint1)
        {
            triagedCorrectCount++;
            needsTriagePoint1 = false;
        }

        if (pickCorrectColor2 && needsTriagePoint2)
        {
            triagedCorrectCount++;
            needsTriagePoint2 = false;
        }

        if (pickCorrectColor3 && needsTriagePoint3)
        {
            triagedCorrectCount++;
            needsTriagePoint3 = false;
        }

        if (pickCorrectColor4 && needsTriagePoint4)
        {
            triagedCorrectCount++;
            needsTriagePoint4 = false;
        }

        if (pickCorrectColor5 && needsTriagePoint5)
        {
            triagedCorrectCount++;
            needsTriagePoint5 = false;
        }

        correctTriage.text = triagedCorrectCount.ToString();
        if (GameFases.treatedAllPatients) //treatment order + ABCDE
        {
            for (int i = 0; i < PlayerInteract.patientTreatmentOrder.Count - 1; i++)
            {
                if (PlayerInteract.patientTreatmentOrder[i] >= PlayerInteract.patientTreatmentOrder[i + 1])
                {
                    treatedInCorrectOrder = true;
                }
                else
                {
                    break;
                }
            }
            if (treatedInCorrectOrder)
            {
                treatedmark.sprite = checkmark;
            }
        }

        for (int i = 0; i < GameTimer.patient1Checks.Count - 1; i++)
        {
            if (GameTimer.patient1Checks[i] >= GameTimer.patient1Checks[i + 1])
            {
                patient1ChecksOrder = true;
            }
            else
            {
                break;
            }
        }
        if (patient1ChecksOrder)
        {
            //set feedback
        }

        for (int i = 0; i < GameTimer.patient2Checks.Count - 1; i++)
        {
            if (GameTimer.patient2Checks[i] >= GameTimer.patient2Checks[i + 1])
            {
                patient2ChecksOrder = true;
            }
            else
            {
                break;
            }
        }
        if (patient2ChecksOrder)
        {
            //set feedback
        }

        for (int i = 0; i < GameTimer.patient3Checks.Count - 1; i++)
        {
            if (GameTimer.patient3Checks[i] >= GameTimer.patient3Checks[i + 1])
            {
                patient3ChecksOrder = true;
            }
            else
            {
                break;
            }
        }
        if (patient3ChecksOrder)
        {
            //set feedback
        }

        for (int i = 0; i < GameTimer.patient4Checks.Count - 1; i++)
        {
            if (GameTimer.patient4Checks[i] >= GameTimer.patient4Checks[i + 1])
            {
                patient4ChecksOrder = true;
            }
            else
            {
                break;
            }
        }
        if (patient4ChecksOrder)
        {
            //set feedback
        }

        for (int i = 0; i < GameTimer.patient5Checks.Count - 1; i++)
        {
            if (GameTimer.patient5Checks[i] >= GameTimer.patient5Checks[i + 1])
            {
                patient5ChecksOrder = true;
            }
            else
            {
                break;
            }
        }
        if (patient5ChecksOrder)
        {
            //set feedback
        }

        if (feedback.activeInHierarchy)
        {
            CountCheckOrder();
        }
    }

    public void Next1()
    {
        panelsToLink[0].SetActive(false);
        panelsToLink[1].SetActive(true);
        nextButtons[0].SetActive(false);
        nextButtons[1].SetActive(true);
        backButtons[0].SetActive(false);
        backButtons[1].SetActive(true);
    }

    public void Next2()
    {
        panelsToLink[1].SetActive(false);
        panelsToLink[2].SetActive(true);
        nextButtons[1].SetActive(false);
        nextButtons[2].SetActive(true);
        backButtons[1].SetActive(false);
        backButtons[2].SetActive(true);
    }

    public void Next3()
    {
        if (patients.Length >= 4)
        {
            panelsToLink[2].SetActive(false);
            panelsToLink[3].SetActive(true);
            nextButtons[2].SetActive(false);
            nextButtons[3].SetActive(true);
            backButtons[2].SetActive(false);
            backButtons[3].SetActive(true);
        }
        else
        {
            panelsToLink[2].SetActive(false);
            panelsToLink[0].SetActive(true);
            nextButtons[2].SetActive(false);
            nextButtons[0].SetActive(true);
            backButtons[2].SetActive(false);
            backButtons[0].SetActive(true);
        }
    }

    public void Next4()
    {
        if (patients.Length >= 5)
        {
            panelsToLink[3].SetActive(false);
            panelsToLink[4].SetActive(true);
            nextButtons[3].SetActive(false);
            nextButtons[4].SetActive(true);
            backButtons[3].SetActive(false);
            backButtons[4].SetActive(true);
        }
        else
        {
            panelsToLink[3].SetActive(false);
            panelsToLink[0].SetActive(true);
            nextButtons[3].SetActive(false);
            nextButtons[0].SetActive(true);
            backButtons[3].SetActive(false);
            backButtons[0].SetActive(true);
        }
    }

    public void Next5()
    {
        panelsToLink[4].SetActive(false);
        panelsToLink[0].SetActive(true);
        nextButtons[4].SetActive(false);
        nextButtons[0].SetActive(true);
        backButtons[4].SetActive(false);
        backButtons[0].SetActive(true);
    }

    public void Back1()
    {
        panelsToLink[0].SetActive(false);
        nextButtons[0].SetActive(false);
        backButtons[0].SetActive(false);

        if (patients.Length == 5)
        {
            panelsToLink[4].SetActive(true);
            nextButtons[4].SetActive(true);
            backButtons[4].SetActive(true);
        }
        else if (patients.Length == 4)
        {
            panelsToLink[3].SetActive(true);
            nextButtons[3].SetActive(true);
            backButtons[3].SetActive(true);
        }
        else if (patients.Length == 3)
        {
            panelsToLink[2].SetActive(true);
            nextButtons[2].SetActive(true);
            backButtons[2].SetActive(true);
        }
    }

    public void Back2()
    {
        panelsToLink[1].SetActive(false);
        panelsToLink[0].SetActive(true);
        nextButtons[1].SetActive(false);
        nextButtons[0].SetActive(true);
        backButtons[1].SetActive(false);
        backButtons[0].SetActive(true);
    }

    public void Back3()
    {
        panelsToLink[2].SetActive(false);
        panelsToLink[1].SetActive(true);
        nextButtons[2].SetActive(false);
        nextButtons[1].SetActive(true);
        backButtons[2].SetActive(false);
        backButtons[1].SetActive(true);
    }
    public void Back4()
    {
        panelsToLink[3].SetActive(false);
        panelsToLink[2].SetActive(true);
        nextButtons[3].SetActive(false);
        nextButtons[2].SetActive(true);
        backButtons[3].SetActive(false);
        backButtons[2].SetActive(true);
    }

    public void Back5()
    {
        panelsToLink[4].SetActive(false);
        panelsToLink[3].SetActive(true);
        nextButtons[4].SetActive(false);
        nextButtons[3].SetActive(true);
        backButtons[4].SetActive(false);
        backButtons[3].SetActive(true);
    }

    public void PatientFeedbackButton()
    {
        globalFeedback.SetActive(false);
        patientFeedback.SetActive(true);
    }

    public void GlobalFeedbackButton()
    {
        patientFeedback.SetActive(false);
        globalFeedback.SetActive(true);
    }

    public void CountCheckOrder()
    {
        if (patient1ChecksOrder && patient1Counted)
        {
            checkCounter++;
            patient1Counted = false;
        }
        if (patient2ChecksOrder && patient2Counted)
        {
            checkCounter++;
            patient2Counted = false;
        }
        if (patient3ChecksOrder && patient3Counted)
        {
            checkCounter++;
            patient3Counted = false;
        }
        if (patient4ChecksOrder && patient4Counted)
        {
            checkCounter++;
            patient4Counted = false;
        }
        if (patient5ChecksOrder && patient5Counted)
        {
            checkCounter++;
            patient5Counted = false;
        }

        checkFeedbackText.text = checkCounter.ToString();
    }
}
