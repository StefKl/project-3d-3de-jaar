﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBars : MonoBehaviour
{
    [SerializeField] private Image timerBar1;
    [SerializeField] private Image timerBar2;
    [SerializeField] private Image timerBar3;
    [SerializeField] private Image timerBar4;
    [SerializeField] private Image timerBar5;
    public float healthBarPatient1 = 10f;
    public float healthBarPatient2 = 8f;
    public float healthBarPatient3 = 7f;
    public float healthBarPatient4 = 9f;
    public float healthBarPatient5 = 5f;

    float timeLeft1;
    float timeLeft2;
    float timeLeft3;
    float timeLeft4;
    float timeLeft5;
    void Start()
    {
        timeLeft1 = healthBarPatient1;
        timeLeft2 = healthBarPatient2;
        timeLeft3 = healthBarPatient3;
        timeLeft4 = healthBarPatient4;
        timeLeft5 = healthBarPatient5;
    }

    void Update()
    {
        if (timeLeft1 > 0)
        {
            if (timeLeft1 < healthBarPatient1/4)
            {
                timerBar1.GetComponent<Image>().color = Color.red;
                Debug.Log("UI");
            }
            timeLeft1 -= Time.deltaTime;
            timerBar1.fillAmount = timeLeft1/healthBarPatient1;    
        }
        else
        {
            Time.timeScale = 0; 
        }
        
        if (timeLeft2 > 0)
        {
            if (timeLeft2 < healthBarPatient2/4)
            {
                timerBar2.GetComponent<Image>().color = Color.red;
                Debug.Log("UI");
            }
            timeLeft2 -= Time.deltaTime;
            timerBar2.fillAmount = timeLeft2/healthBarPatient2;    
        }
        else
        {
            Time.timeScale = 0; 
        }      
        
        if (timeLeft3 > 0)
        {
            if (timeLeft3 < healthBarPatient3/4)
            {
                timerBar3.GetComponent<Image>().color = Color.red;
                Debug.Log("UI");
            }
            timeLeft3 -= Time.deltaTime;
            timerBar3.fillAmount = timeLeft3/healthBarPatient3;    
        }
        else
        {
            Time.timeScale = 0; 
        }  

        if (timeLeft4 > 0)
        {
            if (timeLeft4 < healthBarPatient4/4)
            {
                timerBar4.GetComponent<Image>().color = Color.red;
                Debug.Log("UI");
            }
            timeLeft4 -= Time.deltaTime;
            timerBar4.fillAmount = timeLeft4/healthBarPatient4;    
        }
        else
        {
            Time.timeScale = 0; 
        }  

        if (timeLeft5 > 0)
        {
            if (timeLeft5 < healthBarPatient5/4)
            {
                timerBar5.GetComponent<Image>().color = Color.red;
                Debug.Log("UI");
            }
            timeLeft5 -= Time.deltaTime;
            timerBar5.fillAmount = timeLeft5/healthBarPatient5;    
        }
        else
        {
            Time.timeScale = 0; 
        }  
    }
}
