﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private Slider loadingScreen;
    [SerializeField] private GameObject [] mainMenuHide;
    
    public void LoadLevel ()
    {
        StartCoroutine(LoadAsync("A12"));
        loadingScreen.gameObject.SetActive(true);
        foreach (GameObject item in mainMenuHide)
        {
            item.SetActive(false);
        }
    }
    
    IEnumerator LoadAsync (string nameScene)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(nameScene);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            loadingScreen.value = progress;

            yield return null;
        }

    }
}
