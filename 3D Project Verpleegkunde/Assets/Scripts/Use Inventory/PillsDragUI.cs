﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PillsDragUI : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private CanvasGroup canvasGroup;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        //put bandage back in original position
        transform.localPosition = new Vector3(0, -32, 0);
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "MouthDrop")
            {
                Debug.Log(hover);
                Debug.Log("Applied pills");
                break;
            }
            //name every drop image different to find different bandage models
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }
}
