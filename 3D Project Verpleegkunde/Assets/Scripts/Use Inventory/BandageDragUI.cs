﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BandageDragUI : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private CanvasGroup canvasGroup;
    public static GameObject bandageObject = null;
    public static GameObject tempObject = null;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        //put bandage back in original position
        transform.localPosition = new Vector3(0, 78, 0);
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "BandageArmPatient1")
            {
                //activate bandage model
                PlayerInteract.patient.transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:LeftShoulder/mixamorig:LeftArm/mixamorig:LeftForeArm/BandageArm").gameObject.SetActive(true);
                Feedback.hasBandagePatient1 = true;
                break;
            }
            if (hover.name == "BandageLegPatient2")
            {
                //activate bandage model
                PlayerInteract.patient.transform.Find("mixamorig:Hips/mixamorig:LeftUpLeg/mixamorig:LeftLeg/BandageLeg").gameObject.SetActive(true);
                Feedback.hasBandagePatient2 = true;
                break;
            }
            if (hover.name == "BandageArmPatient4")
            {
                //activate bandage model
                PlayerInteract.patient.transform.Find("Armature/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/BandageArm").gameObject.SetActive(true);
                Feedback.hasBandagePatient4 = true;
                break;
            }
            if (hover.name == "BandageArmPatient5")
            {
                //activate bandage model
                PlayerInteract.patient.transform.Find("Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/BandageArm").gameObject.SetActive(true);
                Feedback.hasBandagePatient5 = true;
                break;
            }
            //name every drop image different to find different bandage models
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }
}
