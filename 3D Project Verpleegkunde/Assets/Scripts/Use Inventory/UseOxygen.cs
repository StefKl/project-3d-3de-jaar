﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UseOxygen : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private CanvasGroup canvasGroup;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        //put bandage back in original position
        transform.localPosition = new Vector3(0, -62, 0);
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "OxygenDropPatient1")
            {
                PlayerInteract.patient.transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/zuurstoffleswitharig").gameObject.SetActive(true);
                Feedback.hasOxygenPatient1 = true;
                break;
            }
            if (hover.name == "OxygenDropPatient2")
            {
                break;
            }
            //name every drop image different to find different bandage models
        }
    }
}
