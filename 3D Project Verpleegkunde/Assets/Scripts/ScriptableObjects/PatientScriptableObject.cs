﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public enum Genders
{
    Male,
    Female
}

public enum SkinColor
{
    white,
    black,
    brown,
}

public enum BreathingPatern
{
    Fast,
    Slow,
    Deep,
    Shallow,
    Not
}

public enum Conscious
{
    Alert,
    [Description("Verbal response")] VerbalResponse,
    [Description("Pain response")] PainResponse,
    Unresponsive
}

public enum Triage
{
    None,
    Red,
    Black,
    Orange,
    Yellow,
    Green,
    Blue
}

public enum BloodType
{
    APositive,
    ANegative,
    BPositive,
    BNegative,
    AbPositive,
    AbNegative,
    OPositive,
    ONegative
}

public enum Pulse
{
    Regular,
    Fast,
    Slow,
    None
}

[CreateAssetMenu(fileName = "New patient", menuName = "Patient/Patient")]
public class PatientScriptableObject : ScriptableObject
{
    public string patientName;
    public Genders gender;
    public int age;
    public int sizeInCm;
    public SkinColor skinColor;
    public BloodType bloodType;
    public BreathingPatern breathing;
    public Conscious conscious;
    public Triage triageColor;
    public Pulse pulse;
    public Sprite photoOfPatient;
    public float healthBarTimer;
    public int urgency;
}
