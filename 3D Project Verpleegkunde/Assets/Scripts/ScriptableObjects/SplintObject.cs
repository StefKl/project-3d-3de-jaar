﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Splint", menuName = "Inventory System/Items/Splint")]
public class SplintObject : ItemObject
{
    public void Awake()
    {
        type = ItemType.Splint;
    }
}
