﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Oxygen", menuName = "Inventory System/Items/Oxygen")]
public class OxygenObject : ItemObject
{
    private void Awake()
    {
        type = ItemType.Oxygen;
    }
}
