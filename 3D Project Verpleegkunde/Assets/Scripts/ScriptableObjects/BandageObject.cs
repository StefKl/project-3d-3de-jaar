﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Bandage", menuName = "Inventory System/Items/Bandage")]
public class BandageObject : ItemObject
{
    public bool isBleeding = true;
    public void Awake()
    {
        type = ItemType.Bandage;
    }
}
