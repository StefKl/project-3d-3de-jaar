﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Syringe", menuName = "Inventory System/Items/Syringe")]
public class SyringeObject : ItemObject
{
    public int painReduction;
    public void Awake()
    {
        type = ItemType.Syringe;
    }
}
