﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerInteractTutorial : MonoBehaviour
{
    [SerializeField] private GameObject inventory;
    [SerializeField] private float raycastDistance;
    [SerializeField] private InventoryObject inventoryObject;
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject reticle;
    [SerializeField] private GameObject patientInteractMenu;
    
    [SerializeField] private GameObject briefingMenu;
    [SerializeField] private GameObject interactItem;
    [SerializeField] private GameObject interactPatient;
    [SerializeField] private GameObject interactButton;
    //[SerializeField] private GameObject interactBriefing;
    [SerializeField] private GameObject feedback;
    [SerializeField] private GameObject mainUI;
    [SerializeField] private GameObject pauseMenu;

    [SerializeField] private Material black;
    [SerializeField] private Material red;
    [SerializeField] private Material orange;
    [SerializeField] private Material yellow;
    [SerializeField] private Material green;
    [SerializeField] private Material blue;

    [SerializeField] private Camera playerCamera;

    [SerializeField] private GameObject[] patients;
    public static int patientCount;
    [SerializeField] private GameObject clipboard;

    //[SerializeField] private GameObject briefing;
    //[SerializeField] private Camera briefingCamera;
    [SerializeField] private TMP_Text numberOfPatientsText;

    [SerializeField] private GameObject[] spawnpoints;
    [SerializeField] private List<GameObject> patientsList;

    [SerializeField] private GameObject patientHeadZoomMenu;
    [SerializeField] private GameObject patientHandZoomMenu;
    [SerializeField] private GameObject patientLegZoomMenu;


    [SerializeField] private GameObject[] patientInjuryUI;
    [SerializeField] private GameObject[] patientInteractUIToHide;
    [SerializeField] private GameObject[] patient1InteractUIToHide;
    [SerializeField] private GameObject[] patient2InteractUIToHide;
    [SerializeField] private GameObject[] patient3InteractUIToHide;
    [SerializeField] private GameObject[] patient4InteractUIToHide;
    [SerializeField] private GameObject[] patient5InteractUIToHide;
    [SerializeField] private GameObject patient1Interact;
    [SerializeField] private GameObject patient2Interact;
    [SerializeField] private GameObject patient3Interact;
    [SerializeField] private GameObject patient4Interact;
    [SerializeField] private GameObject patient5Interact;

    [SerializeField] private int minimumPatients = 0;

    [SerializeField] private GameObject patientControlsTutorial1;
    [SerializeField] private GameObject patientControlsTutorial2;
    [SerializeField] private GameObject patientControlsTutorial3;

    private RaycastHit hit;
    private GameObject temporaryCamera;
    private GameObject temporaryHeadCamera;
    private GameObject temporaryHandCamera;
    private GameObject temporaryLegCamera;
    public static GameObject[] patientInfoPrefabArray;

    private int patientsToSpawn;
    private int indexOfPrefab;
    private GameObject temporaryPatient;
    private bool pickedUpBackpack = false;

    [HideInInspector] public static Patient patient;
    [HideInInspector] public PatientInteraction patientInteraction;

    void Awake()
    {
        //enable controls tutorial guide

        playerCamera.enabled = true;
        //calculate random number of patients
        patientsToSpawn = Random.Range(minimumPatients, spawnpoints.Length + 1);

        //set briefing text
        numberOfPatientsText.text = patientsToSpawn.ToString();

        //instantiate generated number of patient prefabs on spawn points
        for (int i = 0; i < patientsToSpawn; i++)
        {
            indexOfPrefab = Random.Range(0, patientsList.Count);
            temporaryPatient = Instantiate(patientsList[indexOfPrefab], spawnpoints[i].transform.position, spawnpoints[i].transform.localRotation);
            temporaryPatient.transform.SetParent(spawnpoints[i].transform, true);
            temporaryPatient.name = "Patient " + (i + 1);
            //set right injurie drops active with patient
            patientsList.RemoveAt(indexOfPrefab);
        }

        foreach (InventorySlot item in inventoryObject.Container)
        {
            item.amount = 0;
        }
    }

    void Start()
    {
        patients = GameObject.FindGameObjectsWithTag("Patient");
        patientCount = patients.Length;
        patientInteraction = new PatientInteraction();
        playerCamera.GetComponent<AudioListener>().enabled = false;
        reticle.SetActive(false);
    }

    void Update()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, raycastDistance))
        {
            Debug.DrawRay(transform.position, transform.forward, Color.green, raycastDistance);
            // Press [e] to interact in the UI
            if (hit.collider.gameObject.tag == "PickUp")
            {
                interactItem.SetActive(true);
            }  
            if (hit.collider.gameObject.tag == "Patient")
            {
                interactPatient.SetActive(true);
            }
            if (hit.collider.gameObject.tag == "Button")
            {
                interactButton.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (hit.collider.gameObject.tag == "PickUp")
                {
                    pickedUpBackpack = true;
                    Destroy(hit.collider.gameObject);
                }

                if (hit.collider.gameObject.tag == "Patient")
                {
                    reticle.SetActive(false);
                    patientInteractMenu.SetActive(true);
                    if (pickedUpBackpack)
                    {
                        inventory.SetActive(true);
                    }
                    patient = hit.collider.gameObject.GetComponent<Patient>();
                    Debug.Log(patient);

                    temporaryCamera = hit.collider.gameObject.transform.GetChild(0).gameObject;
                    MoveCamera(temporaryCamera);

                    for (int i = 0; i < patients.Length + 1; i++)
                    {
                        if (hit.collider.gameObject.name == "Patient " + (i + 1))
                        {
                            //set this patient injury UI on
                            if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                            {
                                Debug.Log("Patient1InjuryUI");
                                //set patient 1 injury UI active
                                //patientInteractMenu.SetActive(true);
                                patient1Interact.SetActive(true);

                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 1 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                            {
                                Debug.Log("Patient2InjuryUI");
                                //set patient 2 injury UI active
                                //patientInteractMenu.SetActive(true);
                                patient2Interact.SetActive(true);

                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 2 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                            {
                                Debug.Log("Patient3InjuryUI");
                                //set patient 3 injury UI active
                                //patient3InteractMenu.SetActive(true);
                                patient3Interact.SetActive(true);
                                //Debug.Log(animator.name);

                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 3 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                            {
                                Debug.Log("Patient4InjuryUI");
                                //set patient 4 injury UI active
                                //patient4InteractMenu.SetActive(true);
                                patient4Interact.SetActive(true);

                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 4 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }
                            }
                            else if (hit.collider.gameObject.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                            {
                                Debug.Log("Patient5InjuryUI");
                                //set patient 5 injury UI active
                                //patient5InteractMenu.SetActive(true);
                                patient5Interact.SetActive(true);

                                for (int j = 0; j < patientInjuryUI.Length; j++)
                                {
                                    if (patientInjuryUI[j].name == "Patient 5 Injuries")
                                    {
                                        patientInjuryUI[j].SetActive(true);
                                    }
                                }
                            }
                        }
                    }
                }

                if (hit.collider.gameObject.tag == "Button")
                {
                    feedback.SetActive(true);
                    Debug.Log("You ended the game.");
                }                
            }
        }

        if (!Physics.Raycast(transform.position, transform.forward, out hit, raycastDistance) || hit.collider.gameObject.tag == "Wall")
        {
            //To prevent it from detecting objects
            interactItem.SetActive(false);
            interactPatient.SetActive(false);
            interactButton.SetActive(false);
            //interactBriefing.SetActive(false);
        }

        if (patientInteractMenu.activeInHierarchy || briefingMenu.activeInHierarchy ||  feedback.activeInHierarchy||startMenu.activeInHierarchy || patientControlsTutorial1.activeInHierarchy || patientControlsTutorial2.activeInHierarchy || patientControlsTutorial3.activeInHierarchy)
        {
            interactItem.SetActive(false);
            interactPatient.SetActive(false);
            interactButton.SetActive(false);
            //interactBriefing.SetActive(false);
            mainUI.SetActive(false);
            this.gameObject.GetComponent<FirstPersonController>().enabled = false;
            this.gameObject.GetComponent<PauseMenuTutorial>().enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            //mainUI.SetActive(true);
            this.gameObject.GetComponent<FirstPersonController>().enabled = true;
            this.gameObject.GetComponent<PauseMenuTutorial>().enabled = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (feedback.activeInHierarchy)
        {
            playerCamera.GetComponent<AudioListener>().enabled = false;
        }
    }

    private void OnApplicationQuit()
    {
        foreach (InventorySlot item in inventoryObject.Container)
        {
            item.amount = 0;
        }
    }

    public void PatientMenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        patientInteractMenu.SetActive(false);
        //inventory false
        clipboard.SetActive(false);
        inventory.SetActive(false);
        reticle.SetActive(true);
        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }

        patient1Interact.SetActive(false);
        patient2Interact.SetActive(false);
        patient3Interact.SetActive(false);
        patient4Interact.SetActive(false);
        patient5Interact.SetActive(false);
    }
    public void Patient3MenuBackButton()
    {
        MoveCameraBack(temporaryCamera);
        //inventory false
        inventory.SetActive(false);
        reticle.SetActive(true);

        foreach (GameObject item in patientInjuryUI)
        {
            item.SetActive(false);
        }
    }

    public void BriefingStart()
    {
        //set first person camera active
        briefingMenu.SetActive(false);
        playerCamera.GetComponent<AudioListener>().enabled = true;
        //set retilce active
        reticle.SetActive(true);
    }

    public void BriefingBackButton()
    {
        MoveBriefingCameraBack(temporaryCamera);
        briefingMenu.SetActive(false);
        reticle.SetActive(true);
    }

    private void MoveCameraToBriefing(GameObject cameraObject)
    {
        playerCamera.gameObject.SetActive(false);
        cameraObject.gameObject.SetActive(true);
    }

    private void MoveBriefingCameraBack(GameObject cameraObject)
    {
        cameraObject.gameObject.SetActive(false);
        playerCamera.gameObject.SetActive(true);
    }
    public void MoveHeadCamera()
    {
        temporaryHeadCamera = hit.collider.gameObject.transform.GetChild(1).gameObject;
        temporaryHeadCamera.gameObject.SetActive(true);
        playerCamera.gameObject.SetActive(false);
        //set patient interact off except for clipboard
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(false);
        }
                foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(false);
        }
    }

    public void MoveHandCamera()
    {
        temporaryHandCamera = hit.collider.gameObject.transform.GetChild(2).gameObject;
        temporaryHandCamera.gameObject.SetActive(true);
        playerCamera.gameObject.SetActive(false);
        //set patient interact off except for clipboard
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(false);
        }
    }
    public void MoveLegCamera()
    {
        temporaryLegCamera = hit.collider.gameObject.transform.GetChild(3).gameObject;
        temporaryLegCamera.gameObject.SetActive(true);
        playerCamera.gameObject.SetActive(false);
        //set patient interact off except for clipboard
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(false);
        }
    }

    public void MoveZoomCameraHandBack()
    {
        MoveCamera(temporaryCamera);
        temporaryHandCamera.gameObject.SetActive(false);
        //set patient interact on
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(true);
        }
    }

    public void MoveZoomCameraHeadBack()
    {
        MoveCamera(temporaryCamera);
        temporaryHeadCamera.gameObject.SetActive(false);
        //set patient interact on
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(true);
        }
    }

    public void MoveZoomCameraLegBack()
    {
        MoveCamera(temporaryCamera);
        temporaryLegCamera.gameObject.SetActive(false);
        //set patient interact on
        foreach (GameObject item in patientInteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient1InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient2InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient3InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient4InteractUIToHide)
        {
            item.SetActive(true);
        }
        foreach (GameObject item in patient5InteractUIToHide)
        {
            item.SetActive(true);
        }
    }

    public void MoveCamera(GameObject cameraObject)
    {
        playerCamera.gameObject.SetActive(false);
        cameraObject.gameObject.SetActive(true);
    }

    private void MoveCameraBack(GameObject cameraObject)
    {
        cameraObject.gameObject.SetActive(false);
        playerCamera.gameObject.SetActive(true);
    }

    #region Radial Buttons Color

    public void RedButton()
    {
        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            patient1Interact.transform.Find("TriageImage1").GetComponent<Image>().color = red.color;
            FeedbackTutorial.hasPickAColor1 = true;
            FeedbackTutorial.pickedColor1 = red.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            patient2Interact.transform.Find("TriageImage2").GetComponent<Image>().color = red.color;
            FeedbackTutorial.hasPickAColor2 = true;
            FeedbackTutorial.pickedColor2 = red.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            patient3Interact.transform.Find("TriageImage3").GetComponent<Image>().color = red.color;
            FeedbackTutorial.hasPickAColor3 = true;
            FeedbackTutorial.pickedColor3 = red.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            patient4Interact.transform.Find("TriageImage4").GetComponent<Image>().color = red.color;
            FeedbackTutorial.hasPickAColor4 = true;
            FeedbackTutorial.pickedColor4 = red.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            patient5Interact.transform.Find("TriageImage5").GetComponent<Image>().color = red.color;
            FeedbackTutorial.hasPickAColor5 = true;
            FeedbackTutorial.pickedColor5 = red.color;
        }
        PatientInteractionTutorial.hasClickedOnButton = true;
    }

    public void BlackButton()
    {
        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            patient1Interact.transform.Find("TriageImage1").GetComponent<Image>().color = black.color;
            FeedbackTutorial.hasPickAColor1 = true;
            FeedbackTutorial.pickedColor1 = black.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            patient2Interact.transform.Find("TriageImage2").GetComponent<Image>().color = black.color;
            FeedbackTutorial.hasPickAColor2 = true;
            FeedbackTutorial.pickedColor2 = black.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            patient3Interact.transform.Find("TriageImage3").GetComponent<Image>().color = black.color;
            FeedbackTutorial.hasPickAColor3 = true;
            FeedbackTutorial.pickedColor3 = black.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            patient4Interact.transform.Find("TriageImage4").GetComponent<Image>().color = black.color;
            FeedbackTutorial.hasPickAColor4 = true;
            FeedbackTutorial.pickedColor4 = black.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            patient5Interact.transform.Find("TriageImage5").GetComponent<Image>().color = black.color;
            FeedbackTutorial.hasPickAColor5 = true;
            FeedbackTutorial.pickedColor5 = black.color;
        }
        PatientInteractionTutorial.hasClickedOnButton = true;
    }

    public void OrangeButton()
    {
        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            patient1Interact.transform.Find("TriageImage1").GetComponent<Image>().color = orange.color;
            FeedbackTutorial.hasPickAColor1 = true;
            FeedbackTutorial.pickedColor1 = orange.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            patient2Interact.transform.Find("TriageImage2").GetComponent<Image>().color = orange.color;
            FeedbackTutorial.hasPickAColor2 = true;
            FeedbackTutorial.pickedColor2 = orange.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            patient3Interact.transform.Find("TriageImage3").GetComponent<Image>().color = orange.color;
            FeedbackTutorial.hasPickAColor3 = true;
            FeedbackTutorial.pickedColor3 = orange.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            patient4Interact.transform.Find("TriageImage4").GetComponent<Image>().color = orange.color;
            FeedbackTutorial.hasPickAColor4 = true;
            FeedbackTutorial.pickedColor4 = orange.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            patient5Interact.transform.Find("TriageImage5").GetComponent<Image>().color = orange.color;
            FeedbackTutorial.hasPickAColor5 = true;
            FeedbackTutorial.pickedColor5 = orange.color;
        }
        PatientInteractionTutorial.hasClickedOnButton = true;
    }

    public void YellowButton()
    {
        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            patient1Interact.transform.Find("TriageImage1").GetComponent<Image>().color = yellow.color;
            FeedbackTutorial.hasPickAColor1 = true;
            FeedbackTutorial.pickedColor1 = yellow.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            patient2Interact.transform.Find("TriageImage2").GetComponent<Image>().color = yellow.color;
            FeedbackTutorial.hasPickAColor2 = true;
            FeedbackTutorial.pickedColor2 = yellow.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            patient3Interact.transform.Find("TriageImage3").GetComponent<Image>().color = yellow.color;
            FeedbackTutorial.hasPickAColor3 = true;
            FeedbackTutorial.pickedColor3 = yellow.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            patient4Interact.transform.Find("TriageImage4").GetComponent<Image>().color = yellow.color;
            FeedbackTutorial.hasPickAColor4 = true;
            FeedbackTutorial.pickedColor4 = yellow.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            patient5Interact.transform.Find("TriageImage5").GetComponent<Image>().color = yellow.color;
            FeedbackTutorial.hasPickAColor5 = true;
            FeedbackTutorial.pickedColor5 = yellow.color;
        }
        PatientInteractionTutorial.hasClickedOnButton = true;
    }

    public void GreenButton()
    {
        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            patient1Interact.transform.Find("TriageImage1").GetComponent<Image>().color = green.color;
            FeedbackTutorial.hasPickAColor1 = true;
            FeedbackTutorial.pickedColor1 = green.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            patient2Interact.transform.Find("TriageImage2").GetComponent<Image>().color = green.color;
            FeedbackTutorial.hasPickAColor2 = true;
            FeedbackTutorial.pickedColor2 = green.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            patient3Interact.transform.Find("TriageImage3").GetComponent<Image>().color = green.color;
            FeedbackTutorial.hasPickAColor3 = true;
            FeedbackTutorial.pickedColor3 = green.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            patient4Interact.transform.Find("TriageImage4").GetComponent<Image>().color = green.color;
            FeedbackTutorial.hasPickAColor4 = true;
            FeedbackTutorial.pickedColor4 = green.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            patient5Interact.transform.Find("TriageImage5").GetComponent<Image>().color = green.color;
            FeedbackTutorial.hasPickAColor5 = true;
            FeedbackTutorial.pickedColor5 = green.color;
        }
        PatientInteractionTutorial.hasClickedOnButton = true;
    }

    public void BlueButton()
    {
        if (patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            patient1Interact.transform.Find("TriageImage1").GetComponent<Image>().color = blue.color;
            FeedbackTutorial.hasPickAColor1 = true;
            FeedbackTutorial.pickedColor1 = blue.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            patient2Interact.transform.Find("TriageImage2").GetComponent<Image>().color = blue.color;
            FeedbackTutorial.hasPickAColor2 = true;
            FeedbackTutorial.pickedColor2 = blue.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            patient3Interact.transform.Find("TriageImage3").GetComponent<Image>().color = blue.color;
            FeedbackTutorial.hasPickAColor3 = true;
            FeedbackTutorial.pickedColor3 = blue.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            patient4Interact.transform.Find("TriageImage4").GetComponent<Image>().color = blue.color;
            FeedbackTutorial.hasPickAColor4 = true;
            FeedbackTutorial.pickedColor4 = blue.color;
        }
        else if (patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            patient5Interact.transform.Find("TriageImage5").GetComponent<Image>().color = blue.color;
            FeedbackTutorial.hasPickAColor5 = true;
            FeedbackTutorial.pickedColor5 = blue.color;
        }
        PatientInteractionTutorial.hasClickedOnButton = true;
    }

    public void EndGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
    #endregion
}