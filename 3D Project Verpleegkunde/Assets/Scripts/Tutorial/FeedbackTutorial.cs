﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackTutorial : MonoBehaviour
{
    [SerializeField] private Sprite patient1;
    [SerializeField] private Sprite patient2;
    [SerializeField] private Sprite patient3;
    [SerializeField] private Sprite patient4;
    [SerializeField] private Sprite patient5;
    [SerializeField] private List<Image> imagesToFill;

    [SerializeField] private GameObject patient1Text;
    [SerializeField] private GameObject patient2Text;
    [SerializeField] private GameObject patient3Text;
    [SerializeField] private GameObject patient4Text;
    [SerializeField] private GameObject patient5Text;

    [SerializeField] private Image pickedColor1Image;
    [SerializeField] private Image pickedColor2Image;
    [SerializeField] private Image pickedColor3Image;
    [SerializeField] private Image pickedColor4Image;
    [SerializeField] private Image pickedColor5Image;

    [SerializeField] private Image triageColor1;
    [SerializeField] private Image triageColor2;
    [SerializeField] private Image triageColor3;
    [SerializeField] private Image triageColor4;
    [SerializeField] private Image triageColor5;

    [SerializeField] private Sprite checkmark;
    [SerializeField] private Sprite cross;

    [SerializeField] private Vector2[] textPositions;
    [SerializeField] private Vector2[] checkPositions;

    [SerializeField] private Material red;
    [SerializeField] private Material black;
    [SerializeField] private Material blue;
    [SerializeField] private Material green;
    [SerializeField] private Material yellow;
    [SerializeField] private Material orange;

    [SerializeField] private Image finalCheck1;
    [SerializeField] private Image finalCheck2;
    [SerializeField] private Image finalCheck3;
    [SerializeField] private Image finalCheck4;
    [SerializeField] private Image finalCheck5;

    #region patientInteractChecks
    public static bool hasBandagePatient1 = false;
    public static bool hasBandagePatient2 = false;
    public static bool hasSplintPatient3 = false;
    public static bool hasBandagePatient4 = false;
    public static bool hasBandagePatient5 = false;

    public static bool checkedConscious1 = false;
    public static bool checkedPulse1 = false;
    public static bool checkedBreathing1 = false;

    public static bool checkedConscious2 = false;
    public static bool checkedPulse2 = false;
    public static bool checkedBreathing2 = false;

    public static bool checkedConscious3 = false;
    public static bool checkedPulse3 = false;
    public static bool checkedBreathing3 = false;

    public static bool checkedConscious4 = false;
    public static bool checkedPulse4 = false;
    public static bool checkedBreathing4 = false;

    public static bool checkedConscious5 = false;
    public static bool checkedPulse5 = false;
    public static bool checkedBreathing5 = false;

    public static bool hasPickAColor1 = false;
    public static bool hasPickAColor2 = false;
    public static bool hasPickAColor3 = false;
    public static bool hasPickAColor4 = false;
    public static bool hasPickAColor5 = false;

    public static Color pickedColor1;
    public static Color pickedColor2;
    public static Color pickedColor3;
    public static Color pickedColor4;
    public static Color pickedColor5;
    #endregion

    private GameObject[] patients;
    private Color zeroAlpha = new Color(0f, 0f, 0f, 0f);
    private Color oneAlpha = new Color(255f, 255f, 255f, 1f);

    void Awake()
    {
        hasBandagePatient1 = false;
        hasBandagePatient2 = false;
        hasSplintPatient3 = false;
        hasBandagePatient4 = false;
        hasBandagePatient5 = false;

        checkedConscious1 = false;
        checkedPulse1 = false;
        checkedBreathing1 = false;

        checkedConscious2 = false;
        checkedPulse2 = false;
        checkedBreathing2 = false;

        checkedConscious3 = false;
        checkedPulse3 = false;
        checkedBreathing3 = false;

        checkedConscious4 = false;
        checkedPulse4 = false;
        checkedBreathing4 = false;

        checkedConscious5 = false;
        checkedPulse5 = false;
        checkedBreathing5 = false;

        hasPickAColor1 = false;
        hasPickAColor2 = false;
        hasPickAColor3 = false;
        hasPickAColor4 = false;
        hasPickAColor5 = false;
    }

    void Start()
    {
        patients = GameObject.FindGameObjectsWithTag("Patient");
        Debug.Log(patients.Length);
        for (int i = 0; i < patients.Length; i++)
        {
            if (imagesToFill[i].sprite == null)
            {
                if (patients[i].GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
                {
                    imagesToFill[i].sprite = patient1;
                    imagesToFill[i].color = oneAlpha;
                    patient1Text.transform.localPosition = textPositions[i];
                    finalCheck1.transform.localPosition = checkPositions[i];

                    if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                    {
                        triageColor1.color = black.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                    {
                        triageColor1.color = blue.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                    {
                        triageColor1.color = green.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                    {
                        triageColor1.color = orange.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                    {
                        triageColor1.color = red.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                    {
                        triageColor1.color = yellow.color;
                    }
                    
                    patient1Text.SetActive(true);
                }
                else if (patients[i].GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
                {
                    imagesToFill[i].sprite = patient2;
                    imagesToFill[i].color = oneAlpha;
                    patient2Text.transform.localPosition = textPositions[i];
                    finalCheck2.transform.localPosition = checkPositions[i];

                    if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                    {
                        triageColor2.color = black.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                    {
                        triageColor2.color = blue.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                    {
                        triageColor2.color = green.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                    {
                        triageColor2.color = orange.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                    {
                        triageColor2.color = red.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                    {
                        triageColor2.color = yellow.color;
                    }

                    patient2Text.SetActive(true);
                }
                else if (patients[i].GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
                {
                    imagesToFill[i].sprite = patient3;
                    imagesToFill[i].color = oneAlpha;
                    patient3Text.transform.localPosition = textPositions[i];
                    finalCheck3.transform.localPosition = checkPositions[i];

                    if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                    {
                        triageColor3.color = black.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                    {
                        triageColor3.color = blue.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                    {
                        triageColor3.color = green.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                    {
                        triageColor3.color = orange.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                    {
                        triageColor3.color = red.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                    {
                        triageColor3.color = yellow.color;
                    }
                    patient3Text.SetActive(true);
                }
                else if (patients[i].GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
                {
                    imagesToFill[i].sprite = patient4;
                    imagesToFill[i].color = oneAlpha;
                    patient4Text.transform.localPosition = textPositions[i];
                    finalCheck4.transform.localPosition = checkPositions[i];

                    if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                    {
                        triageColor4.color = black.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                    {
                        triageColor4.color = blue.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                    {
                        triageColor4.color = green.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                    {
                        triageColor4.color = orange.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                    {
                        triageColor4.color = red.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                    {
                        triageColor4.color = yellow.color;
                    }

                    patient4Text.SetActive(true);
                }
                else if (patients[i].GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
                {
                    imagesToFill[i].sprite = patient5;
                    imagesToFill[i].color = oneAlpha;
                    patient5Text.transform.localPosition = textPositions[i];
                    finalCheck5.transform.localPosition = checkPositions[i];

                    if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Black)
                    {
                        triageColor5.color = black.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Blue)
                    {
                        triageColor5.color = blue.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Green)
                    {
                        triageColor5.color = green.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Orange)
                    {
                        triageColor5.color = orange.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Red)
                    {
                        triageColor5.color = red.color;
                    }
                    else if (patients[i].GetComponent<Patient>().patientInfo.triageColor == Triage.Yellow)
                    {
                        triageColor5.color = yellow.color;
                    }

                    patient5Text.SetActive(true);
                }
            }
        }

        foreach (Image item in imagesToFill)
        {
            if (item.sprite == null)
            {
                item.color = zeroAlpha;
            }
        }
    }

    void Update()
    {
        for (int i = 0; i < patients.Length; i++)
        {
            if (patients[i].GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
            {
                if (checkedConscious1)
                {
                    patient1Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing1)
                {
                    patient1Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse1)
                {
                    patient1Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient1)
                {
                    patient1Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient1Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor1)
                {
                    pickedColor1Image.color = pickedColor1;
                    pickedColor1Image.sprite = null;
                }

                if (patient1Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient1Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient1Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient1Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor1)
                                {
                                    if (pickedColor1 == triageColor1.color)
                                    {
                                        finalCheck1.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
            {
                if (checkedConscious2)
                {
                    patient2Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing2)
                {
                    patient2Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse2)
                {
                    patient2Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient2)
                {
                    patient2Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient2Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor2)
                {
                    pickedColor2Image.color = pickedColor2;
                    pickedColor2Image.sprite = null;
                }

                if (patient2Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient2Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient2Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient2Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor2)
                                {
                                    if (pickedColor2 == triageColor2.color)
                                    {
                                        finalCheck2.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
            {
                if (checkedConscious3)
                {
                    patient3Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing3)
                {
                    patient3Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse3)
                {
                    patient3Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasSplintPatient3)
                {
                    patient3Text.transform.Find("SplintMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient3Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor3)
                {
                    pickedColor3Image.color = pickedColor3;
                    pickedColor3Image.sprite = null;
                }

                if (patient3Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient3Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient3Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient3Text.transform.Find("SplintMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor3)
                                {
                                    if (pickedColor3 == triageColor3.color)
                                    {
                                        finalCheck3.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
            {
                if (checkedConscious4)
                {
                    patient4Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing4)
                {
                    patient4Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse4)
                {
                    patient4Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient4)
                {
                    patient4Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient4Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor4)
                {
                    pickedColor4Image.color = pickedColor4;
                    pickedColor4Image.sprite = null;
                }

                if (patient4Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient4Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient4Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient4Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor4)
                                {
                                    if (pickedColor4 == triageColor4.color)
                                    {
                                        finalCheck4.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
            {
                if (checkedConscious5)
                {
                    patient5Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedBreathing5)
                {
                    patient5Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (checkedPulse5)
                {
                    patient5Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasBandagePatient5)
                {
                    patient5Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = checkmark;
                }
                else
                {
                    patient5Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite = cross;
                }

                if (hasPickAColor5)
                {
                    pickedColor5Image.color = pickedColor5;
                    pickedColor5Image.sprite = null;
                }

                if (patient5Text.transform.Find("ConsciousMark").gameObject.GetComponent<Image>().sprite == checkmark)
                {
                    if (patient5Text.transform.Find("BreathingMark").gameObject.GetComponent<Image>().sprite == checkmark)
                    {
                        if (patient5Text.transform.Find("PulseMark").gameObject.GetComponent<Image>().sprite == checkmark)
                        {
                            if (patient5Text.transform.Find("BandageMark").gameObject.GetComponent<Image>().sprite == checkmark)
                            {
                                if (hasPickAColor5)
                                {
                                    if (pickedColor5 == triageColor5.color)
                                    {
                                        finalCheck5.sprite = checkmark;
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
