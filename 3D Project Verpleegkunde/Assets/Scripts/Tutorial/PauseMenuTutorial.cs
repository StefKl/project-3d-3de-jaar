﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
public class PauseMenuTutorial : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private bool isPauseMenuActive;
    [SerializeField] private GameObject patientInteractMenu;
    [SerializeField] private GameObject controls;
    [SerializeField] private GameObject[] pauseMenuItemsToHide;

    [SerializeField] private GameObject interactUI;
    [SerializeField] private GameObject mainUI;
    [SerializeField] private GameObject patientInteract;

    [SerializeField] private GameObject[] interactText;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!patientInteractMenu.activeInHierarchy)
            {
                isPauseMenuActive = !isPauseMenuActive;
            }
        }

        if (isPauseMenuActive)
        {
            mainUI.SetActive(false);
            interactUI.SetActive(false);
            patientInteract.SetActive(false);

            foreach (GameObject item in interactText)
            {
                item.SetActive(false);
            }

            this.gameObject.GetComponent<FirstPersonController>().enabled = false;
            this.gameObject.GetComponent<PlayerInteractTutorial>().enabled = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            ActivateMenu();
        }
        else
        {
            mainUI.SetActive(true);
            this.gameObject.GetComponent<FirstPersonController>().enabled = true;
            this.gameObject.GetComponent<PlayerInteractTutorial>().enabled = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            DeactivateMenu();
        }
    }    
    public void ControlsButton()
    {
        foreach (GameObject item in pauseMenuItemsToHide)
        {
            item.SetActive(false);
        }
        controls.SetActive(true);
        Debug.Log("Show controls graphic");
    }

    public void ControlsBackbutton()
    {
        controls.SetActive(false);
        foreach (GameObject item in pauseMenuItemsToHide)
        {
            item.SetActive(true);
        }
    }
    public void Quit()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ActivateMenu()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }
    public void DeactivateMenu()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        isPauseMenuActive = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}

