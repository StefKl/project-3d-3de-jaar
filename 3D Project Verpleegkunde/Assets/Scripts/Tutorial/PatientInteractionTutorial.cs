﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;

public class PatientInteractionTutorial : MonoBehaviour
{
    [SerializeField] private GameObject medicationOptions;
    [SerializeField] private GameObject patientInteractionMenu;
    [SerializeField] private GameObject radialMenu;
    [SerializeField] private GameObject[] patientInteractionMenuItemsToHide;

    [SerializeField] private GameObject patient1Interact;
    [SerializeField] private GameObject patient2Interact;
    [SerializeField] private GameObject patient3Interact;
    [SerializeField] private GameObject patient4Interact;
    [SerializeField] private GameObject patient5Interact;

    [SerializeField] private Material orange;

    public static bool hasClickedOnButton = false;

    public void TriageButton()
    {
        foreach (GameObject item in patientInteractionMenuItemsToHide)
        {
            item.SetActive(false);
        }

        hasClickedOnButton = false;

        radialMenu.SetActive(true);
    }

    void Update()
    {
        if (hasClickedOnButton)
        {
            TriageBackbutton();
        }
    }

    void TriageBackbutton()
    {
        radialMenu.SetActive(false);
        foreach (GameObject item in patientInteractionMenuItemsToHide)
        {
            item.SetActive(true);
        }
    }

    public void ConsciousButton()
    {
        if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            Feedback.checkedConscious1 = true;
            IconHover.checkedConsciousness1=true;
            if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Alert)
            {
                patient1Interact.transform.Find("BrainImage1").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.PainResponse)
            {
                patient1Interact.transform.Find("BrainImage1").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Unresponsive)
            {
                patient1Interact.transform.Find("BrainImage1").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.VerbalResponse)
            {
                patient1Interact.transform.Find("BrainImage1").GetComponent<Image>().color = orange.color;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            Feedback.checkedConscious2 = true;
            IconHover.checkedConsciousness2 = true;
            if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Alert)
            {
                patient2Interact.transform.Find("BrainImage2").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.PainResponse)
            {
                patient2Interact.transform.Find("BrainImage2").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Unresponsive)
            {
                patient2Interact.transform.Find("BrainImage2").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.VerbalResponse)
            {
                patient2Interact.transform.Find("BrainImage2").GetComponent<Image>().color = orange.color;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            Feedback.checkedConscious3 = true;
            IconHover.checkedConsciousness3 = true;
            if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Alert)
            {
                patient3Interact.transform.Find("BrainImage3").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.PainResponse)
            {
                patient3Interact.transform.Find("BrainImage3").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Unresponsive)
            {
                patient3Interact.transform.Find("BrainImage3").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.VerbalResponse)
            {
                patient3Interact.transform.Find("BrainImage3").GetComponent<Image>().color = orange.color;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            Feedback.checkedConscious4 = true;
            IconHover.checkedConsciousness4 = true;
            if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Alert)
            {
                patient4Interact.transform.Find("BrainImage4").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.PainResponse)
            {
                patient4Interact.transform.Find("BrainImage4").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Unresponsive)
            {
                patient4Interact.transform.Find("BrainImage4").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.VerbalResponse)
            {
                patient4Interact.transform.Find("BrainImage4").GetComponent<Image>().color = orange.color;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            Feedback.checkedConscious5 = true;
            IconHover.checkedConsciousness5 = true;
            if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Alert)
            {
                patient5Interact.transform.Find("BrainImage5").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.PainResponse)
            {
                patient5Interact.transform.Find("BrainImage5").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.Unresponsive)
            {
                patient5Interact.transform.Find("BrainImage5").GetComponent<Image>().color = Color.red;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.conscious == Conscious.VerbalResponse)
            {
                patient5Interact.transform.Find("BrainImage5").GetComponent<Image>().color = orange.color;
            }
        }
    }

    public void PulseButton()
    {
        if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            Feedback.checkedPulse1 = true;
            IconHover.checkedPulse1 = true;
            if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Fast)
            {
                patient1Interact.transform.Find("HeartImage1").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Regular)
            {
                patient1Interact.transform.Find("HeartImage1").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Slow)
            {
                patient1Interact.transform.Find("HeartImage1").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.None)
            {
                patient1Interact.transform.Find("HeartImage1").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            Feedback.checkedPulse2 = true;
            IconHover.checkedPulse2 = true;
            if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Fast)
            {
                patient2Interact.transform.Find("HeartImage2").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Regular)
            {
                patient2Interact.transform.Find("HeartImage2").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Slow)
            {
                patient2Interact.transform.Find("HeartImage2").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.None)
            {
                patient2Interact.transform.Find("HeartImage2").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            Feedback.checkedPulse3 = true;
            IconHover.checkedPulse3 = true;
            if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Fast)
            {
                patient3Interact.transform.Find("HeartImage3").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Regular)
            {
                patient3Interact.transform.Find("HeartImage3").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Slow)
            {
                patient3Interact.transform.Find("HeartImage3").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.None)
            {
                patient3Interact.transform.Find("HeartImage3").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            Feedback.checkedPulse4 = true;
            IconHover.checkedPulse4 = true;
            if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Fast)
            {
                patient4Interact.transform.Find("HeartImage4").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Regular)
            {
                patient4Interact.transform.Find("HeartImage4").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Slow)
            {
                patient4Interact.transform.Find("HeartImage4").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.None)
            {
                patient4Interact.transform.Find("HeartImage4").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            Feedback.checkedPulse5 = true;
            IconHover.checkedPulse5 = true;
            if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Fast)
            {
                patient5Interact.transform.Find("HeartImage5").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Regular)
            {
                patient5Interact.transform.Find("HeartImage5").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.Slow)
            {
                patient5Interact.transform.Find("HeartImage5").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.pulse == Pulse.None)
            {
                patient5Interact.transform.Find("HeartImage5").GetComponent<Image>().color = Color.red;
            }
        }
    }

    public void BreathingButton()
    {
        if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            Feedback.checkedBreathing1 = true;
            IconHover.checkedBreathing1 = true;
            if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Deep)
            {
                patient1Interact.transform.Find("LungsImage1").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Fast)
            {
                patient1Interact.transform.Find("LungsImage1").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Shallow)
            {
                patient1Interact.transform.Find("LungsImage1").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Slow)
            {
                patient1Interact.transform.Find("LungsImage1").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Not)
            {
                patient1Interact.transform.Find("LungsImage1").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            Feedback.checkedBreathing2 = true;
            IconHover.checkedBreathing2 = true;
            if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Deep)
            {
                patient2Interact.transform.Find("LungsImage2").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Fast)
            {
                patient2Interact.transform.Find("LungsImage2").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Shallow)
            {
                patient2Interact.transform.Find("LungsImage2").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Slow)
            {
                patient2Interact.transform.Find("LungsImage2").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Not)
            {
                patient2Interact.transform.Find("LungsImage2").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            Feedback.checkedBreathing3 = true;
            IconHover.checkedBreathing3 = true;
            if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Deep)
            {
                patient3Interact.transform.Find("LungsImage3").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Fast)
            {
                patient3Interact.transform.Find("LungsImage3").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Shallow)
            {
                patient3Interact.transform.Find("LungsImage3").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Slow)
            {
                patient3Interact.transform.Find("LungsImage3").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Not)
            {
                patient3Interact.transform.Find("LungsImage3").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            Feedback.checkedBreathing4 = true;
            IconHover.checkedBreathing4 = true;
            if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Deep)
            {
                patient4Interact.transform.Find("LungsImage4").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Fast)
            {
                patient4Interact.transform.Find("LungsImage4").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Shallow)
            {
                patient4Interact.transform.Find("LungsImage4").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Slow)
            {
                patient4Interact.transform.Find("LungsImage4").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Not)
            {
                patient4Interact.transform.Find("LungsImage4").GetComponent<Image>().color = Color.red;
            }
        }
        else if (PlayerInteractTutorial.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            Feedback.checkedBreathing5 = true;
            IconHover.checkedBreathing5 = true;
            if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Deep)
            {
                patient5Interact.transform.Find("LungsImage5").GetComponent<Image>().color = Color.green;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Fast)
            {
                patient5Interact.transform.Find("LungsImage5").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Shallow)
            {
                patient5Interact.transform.Find("LungsImage5").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Slow)
            {
                patient5Interact.transform.Find("LungsImage5").GetComponent<Image>().color = orange.color;
            }
            else if (PlayerInteractTutorial.patient.patientInfo.breathing == BreathingPatern.Not)
            {
                patient5Interact.transform.Find("LungsImage5").GetComponent<Image>().color = Color.red;
            }
        }
    }

    public void PainButton()
    {
        if (PlayerInteractTutorial.patient.patientInfo.conscious != Conscious.Unresponsive)
        {
            medicationOptions.SetActive(true);
        }
        else
        {
            Debug.Log("patient is unconscious and can't react");
        }
    }
    public void BackButton()
    {
        medicationOptions.SetActive(false);
        patientInteractionMenu.SetActive(true);
    }
}

