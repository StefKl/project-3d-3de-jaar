﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SplitDragUITutorial : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private CanvasGroup canvasGroup;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        //put bandage back in original position
        transform.localPosition = new Vector3(0, -102, 0);
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "SplintDrop")
            {
                PlayerInteractTutorial.patient.transform.Find("Armature.003").gameObject.SetActive(false);
                PlayerInteractTutorial.patient.transform.Find("Cube").gameObject.SetActive(false);
                PlayerInteractTutorial.patient.transform.Find("low-polyMesh.002").gameObject.SetActive(false);
                PlayerInteractTutorial.patient.transform.Find("male1591Mesh.003").gameObject.SetActive(false);
                PlayerInteractTutorial.patient.transform.Find("male_casualsuit01Mesh.003").gameObject.SetActive(false);
                PlayerInteractTutorial.patient.transform.Find("shoes04Mesh.003").gameObject.SetActive(false);

                PlayerInteractTutorial.patient.transform.Find("Laying Shaking Head").gameObject.SetActive(true);
                //activate splint model
                PlayerInteractTutorial.patient.transform.Find("Splint").gameObject.SetActive(true);
                Feedback.hasSplintPatient3 = true;
                break;
            }
            //name every drop image different to find different splint models
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }
}

