﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class TutorialGuide : MonoBehaviour
{
    [SerializeField] private GameObject briefing;
    [SerializeField] private GameObject walkingControls;
    [SerializeField] private GameObject pickUpBackpack;
    [SerializeField] private GameObject lookForPatients;
    [SerializeField] private GameObject patientControls1;
    [SerializeField] private GameObject patientControls2;
    [SerializeField] private GameObject patientControls3;
    [SerializeField] private GameObject backpack;
    [SerializeField] private GameObject player;

    private bool hasInteractedWithBackpack = false;
    private bool hasInteractedWithPatient = false;
    private bool hasLookBeenTrue = false;
    private bool hasPatientBeenTrue = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!briefing.activeInHierarchy)
        {
            walkingControls.SetActive(true);
        }

        if (hasInteractedWithBackpack)
        {
            walkingControls.SetActive(false);
            pickUpBackpack.SetActive(true);
        }

        if (backpack == null)
        {
            pickUpBackpack.SetActive(false);
            lookForPatients.SetActive(true);
        }

        if (hasLookBeenTrue)
        {
            lookForPatients.SetActive(false);
        }

        if (hasInteractedWithPatient && !hasPatientBeenTrue)
        {
            lookForPatients.SetActive(false);
            patientControls1.SetActive(true);
            hasLookBeenTrue = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "InteractBackpack")
        {
            hasInteractedWithBackpack = true;
        }

        if (other.gameObject.name == "InteractPatient 1")
        {
            hasInteractedWithPatient = true;
        }

        if (other.gameObject.name == "InteractPatient 2")
        {
            hasInteractedWithPatient = true;
        }
    }

    public void NextButton1Tutorial()
    {
        patientControls1.SetActive(false);
        patientControls2.SetActive(true);
        hasInteractedWithPatient = false;
    }

    public void NextButton2Tutorial()
    {
        patientControls2.SetActive(false);
        patientControls3.SetActive(true);
        hasInteractedWithPatient = false;
    }

    public void ContinueButtonTutorial()
    {
        patientControls3.SetActive(false);
        hasPatientBeenTrue = true;
    }
}
