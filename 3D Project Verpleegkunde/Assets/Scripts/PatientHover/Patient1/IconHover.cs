﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class IconHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject Text;

    public static bool checkedPulse1 = false;
    public static bool checkedPulse2 = false;
    public static bool checkedPulse3 = false;
    public static bool checkedPulse4 = false;
    public static bool checkedPulse5 = false;

    public static bool checkedConsciousness1 = false;
    public static bool checkedConsciousness2 = false;
    public static bool checkedConsciousness3 = false;
    public static bool checkedConsciousness4 = false;
    public static bool checkedConsciousness5 = false;

    public static bool checkedBreathing1 = false;
    public static bool checkedBreathing2 = false;
    public static bool checkedBreathing3 = false;
    public static bool checkedBreathing4 = false;
    public static bool checkedBreathing5 = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (this.name == "HeartImage1" && checkedPulse1)
        {
            Text.SetActive(true);
        }
        if (this.name == "HeartImage2" && checkedPulse2)
        {
            Text.SetActive(true);
        }
        if (this.name == "HeartImage3" && checkedPulse3)
        {
            Text.SetActive(true);
        }
        if (this.name == "HeartImage4" && checkedPulse4)
        {
            Text.SetActive(true);
        }
        if (this.name == "HeartImage5" && checkedPulse5)
        {
            Text.SetActive(true);
        }

        if (this.name == "BrainImage1" && checkedConsciousness1)
        {
            Text.SetActive(true);
        }
        if (this.name == "BrainImage2" && checkedConsciousness2)
        {
            Text.SetActive(true);
        }
        if (this.name == "BrainImage3" && checkedConsciousness3)
        {
            Text.SetActive(true);
        }
        if (this.name == "BrainImage4" && checkedConsciousness4)
        {
            Text.SetActive(true);
        }
        if (this.name == "BrainImage5" && checkedConsciousness5)
        {
            Text.SetActive(true);
        }

        if (this.name == "LungsImage1" && checkedBreathing1)
        {
            Text.SetActive(true);
        }
        if (this.name == "LungsImage2" && checkedBreathing2)
        {
            Text.SetActive(true);
        }
        if (this.name == "LungsImage3" && checkedBreathing3)
        {
            Text.SetActive(true);
        }
        if (this.name == "LungsImage4" && checkedBreathing4)
        {
            Text.SetActive(true);
        }
        if (this.name == "LungsImage5" && checkedBreathing5)
        {
            Text.SetActive(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Text.SetActive(false);
    }
}
