﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P3HoverHead : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private GameObject HeadText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        HeadText.SetActive(true);
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        HeadText.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HandText.SetActive(false);
    }
}

