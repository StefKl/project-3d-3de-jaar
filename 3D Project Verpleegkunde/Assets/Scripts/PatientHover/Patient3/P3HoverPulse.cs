﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P3HoverPulse : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private GameObject PulseText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        PulseText.SetActive(true);
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        PulseText.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HeadText.SetActive(false);
    }
}
