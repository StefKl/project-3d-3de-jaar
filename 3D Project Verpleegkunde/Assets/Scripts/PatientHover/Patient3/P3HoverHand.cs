﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P3HoverHand : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
        [SerializeField] private TextMeshProUGUI Text;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        Text.color = Color.grey;
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        Text.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Text.color = Color.white;
    }
}


