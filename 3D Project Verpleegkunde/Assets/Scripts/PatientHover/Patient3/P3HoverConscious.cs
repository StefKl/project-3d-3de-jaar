﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P3HoverConscious : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private GameObject ConsciousText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        ConsciousText.SetActive(true);
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        ConsciousText.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HeadText.SetActive(false);
    }
}
