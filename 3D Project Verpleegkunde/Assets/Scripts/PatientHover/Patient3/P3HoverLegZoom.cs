﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P3HoverLegZoom : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject LegText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        LegText.SetActive(true);
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        LegText.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HeadText.SetActive(false);
    }
}
