﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P3HoverHeadZoom : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private TextMeshProUGUI ZoomOutText;
    [SerializeField] private TextMeshProUGUI ConsciousText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "ConsciousText")
            {
                ConsciousText.color = Color.grey;
                break;
            }
            if (hover.name == "ZoomOutText")
            {
                ZoomOutText.color = Color.grey;
                break;
            }
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        ZoomOutText.color = Color.white;
        ConsciousText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        ZoomOutText.color = Color.white;
        ConsciousText.color = Color.white;
    }
}


