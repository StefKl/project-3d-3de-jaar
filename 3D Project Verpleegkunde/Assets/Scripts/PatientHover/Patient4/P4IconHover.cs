﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class P4IconHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject pulseText;
    [SerializeField] private GameObject consciousText;
    [SerializeField] private GameObject breathingText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "HeartImage4")
            {
                pulseText.SetActive(true);
            }

            if (hover.name == "BrainImage4")
            {
                consciousText.SetActive(true);
            }

            if (hover.name == "LungsImage4")
            {
                breathingText.SetActive(true);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pulseText.SetActive(false);
        consciousText.SetActive(false);
        breathingText.SetActive(false);
    }
}
