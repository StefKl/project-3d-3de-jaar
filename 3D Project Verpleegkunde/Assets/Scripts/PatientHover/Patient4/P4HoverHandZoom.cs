﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P4HoverHandZoom : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject ZoomOutText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        ZoomOutText.SetActive(true);
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        ZoomOutText.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HeadText.SetActive(false);
    }
}

