﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P4HoverHand : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject HandText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        HandText.SetActive(true);
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        HandText.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HeadText.SetActive(false);
    }
}

