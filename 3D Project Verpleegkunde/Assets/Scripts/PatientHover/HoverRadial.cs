﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class HoverRadial: MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject colorInfo;
    [SerializeField] private GameObject smallButton;
    [SerializeField] private GameObject bigButton;
    
    private Color temporaryColor;

    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        temporaryColor = this.GetComponent<Image>().color;
        temporaryColor.a = 0f;
        this.GetComponent<Image>().color = temporaryColor;
        colorInfo.SetActive(true);
        bigButton.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        temporaryColor = this.GetComponent<Image>().color;
        temporaryColor.a = 0.5f;
        this.GetComponent<Image>().color = temporaryColor;
        colorInfo.SetActive(false);
        bigButton.SetActive(false);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        temporaryColor = this.GetComponent<Image>().color;
        temporaryColor.a = 0.5f;
        this.GetComponent<Image>().color = temporaryColor;
        colorInfo.SetActive(false);
        bigButton.SetActive(false);
    }
}

