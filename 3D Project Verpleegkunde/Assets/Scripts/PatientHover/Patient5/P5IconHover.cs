﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class P5IconHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private GameObject pulseText;
    [SerializeField] private GameObject consciousText;
    [SerializeField] private GameObject breathingText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "HeartImage5")
            {
                pulseText.SetActive(true);
            }

            if (hover.name == "BrainImage5")
            {
                consciousText.SetActive(true);
            }

            if (hover.name == "LungsImage5")
            {
                breathingText.SetActive(true);
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pulseText.SetActive(false);
        consciousText.SetActive(false);
        breathingText.SetActive(false);
    }
}
