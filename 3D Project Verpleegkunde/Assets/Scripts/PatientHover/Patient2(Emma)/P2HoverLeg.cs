﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P2HoverLeg : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private TextMeshProUGUI LegText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        LegText.color = Color.grey;
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        LegText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        LegText.color = Color.white;
    }
}

