﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P2HoverHead : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private TextMeshProUGUI HeadText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        HeadText.color = Color.grey;
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        HeadText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        HeadText.color = Color.white;
    }
}
