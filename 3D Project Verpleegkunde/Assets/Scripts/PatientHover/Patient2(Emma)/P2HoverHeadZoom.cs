﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P2HoverHeadZoom : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
        [SerializeField] private TextMeshProUGUI ZoomOutText;
        [SerializeField] private TextMeshProUGUI ConsciousText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "ConsciousText")
            {
                Debug.Log(hover);
                ConsciousText.color = Color.grey;

                //activate bandage model
                break;
            }
            if (hover.name == "ZoomOutText")
            {
                Debug.Log(hover);
                ZoomOutText.color = Color.grey;
                //activate bandage model
                break;
            }
        }
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        ZoomOutText.color = Color.white;
        ConsciousText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        ZoomOutText.color = Color.white;
        ConsciousText.color = Color.white;
    }
}

