﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P2HoverConscious : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private TextMeshProUGUI ConsciousText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        ConsciousText.color = Color.grey;
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        ConsciousText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //HeadText.SetActive(false);
    }
}
