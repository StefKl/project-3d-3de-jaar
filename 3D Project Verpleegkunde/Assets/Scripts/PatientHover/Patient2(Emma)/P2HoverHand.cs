﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P2HoverHand : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{

    [SerializeField] private TextMeshProUGUI HandText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        HandText.color = Color.grey;
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        HandText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        HandText.color = Color.white;
    }
}

