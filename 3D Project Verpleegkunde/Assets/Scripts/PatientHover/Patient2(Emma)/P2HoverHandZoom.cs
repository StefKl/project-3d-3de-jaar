﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;


public class P2HoverHandZoom : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
        [SerializeField] private TextMeshProUGUI PulseText;


    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        foreach (GameObject hover in eventData.hovered)
        {
            if (hover.name == "PulseText")
            {
                PulseText.color = Color.grey;
                break;
            }
        }
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        PulseText.color = Color.white;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        PulseText.color = Color.white;
    }
}


