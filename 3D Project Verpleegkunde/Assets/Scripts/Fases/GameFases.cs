﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameFases : MonoBehaviour
{
    [SerializeField] private GameObject phase1;
    [SerializeField] private GameObject phase2;

    [SerializeField] private GameObject triageButton;
    [SerializeField] private GameObject[] triageImage;
    [SerializeField] private GameObject phaseScreen;

    public static bool fullyExaminedPatient1 = false;
    public static bool fullyExaminedPatient2 = false;
    public static bool fullyExaminedPatient3 = false;
    public static bool fullyExaminedPatient4 = false;
    public static bool fullyExaminedPatient5 = false;
    public static bool examinedAllPatients = false;

    public static bool fullyTreatedPatient1 = false;
    public static bool fullyTreatedPatient2 = false;
    public static bool fullyTreatedPatient3 = false;
    public static bool fullyTreatedPatient4 = false;
    public static bool fullyTreatedPatient5 = false;
    public static bool treatedAllPatients = false;

    private bool patient1NeedsPointTreatment = true;
    private bool patient2NeedsPointTreatment = true;
    private bool patient3NeedsPointTreatment = true;
    private bool patient4NeedsPointTreatment = true;
    private bool patient5NeedsPointTreatment = true;
    private int patientTreatmentCount;

    private GameObject[] patients;
    private bool patient1InScene = false;
    private bool patient2InScene = false;
    private bool patient3InScene = false;
    private bool patient4InScene = false;
    private bool patient5InScene = false;
    private int patientExaminedCounter;

    private bool patient1NeedsPoint = true;
    private bool patient2NeedsPoint = true;
    private bool patient3NeedsPoint = true;
    private bool patient4NeedsPoint = true;
    private bool patient5NeedsPoint = true;

    private void Awake()
    {

        fullyExaminedPatient1 = false;
        fullyExaminedPatient2 = false;
        fullyExaminedPatient3 = false;
        fullyExaminedPatient4 = false;
        fullyExaminedPatient5 = false;
        examinedAllPatients = false;

        fullyTreatedPatient1 = false;
        fullyTreatedPatient2 = false;
        fullyTreatedPatient3 = false;
        fullyTreatedPatient4 = false;
        fullyTreatedPatient5 = false;
        treatedAllPatients = false;

        patient1InScene = false;
        patient2InScene = false;
        patient3InScene = false;
        patient4InScene = false;
        patient5InScene = false;

        patientExaminedCounter = 0;
        patientTreatmentCount = 0;

        patient1NeedsPoint = true;
        patient2NeedsPoint = true;
        patient3NeedsPoint = true;
        patient4NeedsPoint = true;
        patient5NeedsPoint = true;

        patient1NeedsPointTreatment = true;
        patient2NeedsPointTreatment = true;
        patient3NeedsPointTreatment = true;
        patient4NeedsPointTreatment = true;
        patient5NeedsPointTreatment = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        patients = GameObject.FindGameObjectsWithTag("Patient");
        for (int i = 0; i < patients.Length; i++)
        {
            if (patients[i].GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
            {
                patient1InScene = true;
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
            {
                patient2InScene = true;
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
            {
                patient3InScene = true;
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
            {
                patient4InScene = true;
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
            {
                patient5InScene = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (patients.Length == 3)
        {
            if (patient1InScene && Patient1FullyExamined() && patient1NeedsPoint)
            {
                patientExaminedCounter++;
                patient1NeedsPoint = false;
            }
            if (patient2InScene && Patient2FullyExamined() && patient2NeedsPoint)
            {
                patientExaminedCounter++;
                patient2NeedsPoint = false;
            }
            if (patient3InScene && Patient3FullyExamined() && patient3NeedsPoint)
            {
                patientExaminedCounter++;
                patient3NeedsPoint = false;
            }
            if (patient4InScene && Patient4FullyExamined() && patient4NeedsPoint)
            {
                patientExaminedCounter++;
                patient4NeedsPoint = false;
            }
            if (patient5InScene && Patient5FullyExamined() && patient5NeedsPoint)
            {
                patientExaminedCounter++;
                patient5NeedsPoint = false;
            }

            if (patientExaminedCounter == 3)
            {
                examinedAllPatients = true;                
                phase1.SetActive(false);
                phase2.SetActive(true);
            }

            if (patient1InScene && fullyTreatedPatient1 && patient1NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient1NeedsPointTreatment = false;
            }
            if (patient2InScene && fullyTreatedPatient2 && patient2NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient2NeedsPointTreatment = false;
            }
            if (patient3InScene && fullyTreatedPatient3 && patient3NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient3NeedsPointTreatment = false;
            }
            if (patient4InScene && fullyTreatedPatient4 && patient4NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient4NeedsPointTreatment = false;
            }
            if (patient5InScene && fullyTreatedPatient5 && patient5NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient5NeedsPointTreatment = false;
            }
            if (patientTreatmentCount == 3)
            {
                treatedAllPatients = true;
            }
        }
        else if (patients.Length == 4)
        {
            if (patient1InScene && Patient1FullyExamined() && patient1NeedsPoint)
            {
                patientExaminedCounter++;
                patient1NeedsPoint = false;
            }
            if (patient2InScene && Patient2FullyExamined() && patient2NeedsPoint)
            {
                patientExaminedCounter++;
                patient2NeedsPoint = false;
            }
            if (patient3InScene && Patient3FullyExamined() && patient3NeedsPoint)
            {
                patientExaminedCounter++;
                patient3NeedsPoint = false;
            }
            if (patient4InScene && Patient4FullyExamined() && patient4NeedsPoint)
            {
                patientExaminedCounter++;
                patient4NeedsPoint = false;
            }
            if (patient5InScene && Patient5FullyExamined() && patient5NeedsPoint)
            {
                patientExaminedCounter++;
                patient5NeedsPoint = false;
            }

            if (patientExaminedCounter == 4)
            {
                examinedAllPatients = true;
                phase1.SetActive(false);
                phase2.SetActive(true);
            }

            if (patient1InScene && fullyTreatedPatient1 && patient1NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient1NeedsPointTreatment = false;
            }
            if (patient2InScene && fullyTreatedPatient2 && patient2NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient2NeedsPointTreatment = false;
            }
            if (patient3InScene && fullyTreatedPatient3 && patient3NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient3NeedsPointTreatment = false;
            }
            if (patient4InScene && fullyTreatedPatient4 && patient4NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient4NeedsPointTreatment = false;
            }
            if (patient5InScene && fullyTreatedPatient5 && patient5NeedsPointTreatment)
            {
                patientTreatmentCount++;
                patient5NeedsPointTreatment = false;
            }
            if (patientTreatmentCount == 4)
            {
                treatedAllPatients = true;
            }
        }
        else if (patients.Length == 5)
        {
            if (Patient1FullyExamined() && Patient2FullyExamined() && Patient3FullyExamined() && Patient4FullyExamined() && Patient5FullyExamined())
            {
                examinedAllPatients = true;
                phase1.SetActive(false);
                phase2.SetActive(true);
            }

            if (fullyTreatedPatient1 && fullyTreatedPatient2 && fullyTreatedPatient3 && fullyTreatedPatient4 && fullyTreatedPatient5)
            {
                treatedAllPatients = true;
            }
        }

        if (examinedAllPatients)
        {
            triageButton.SetActive(false);

            foreach (GameObject item in triageImage)
            {
                item.SetActive(false);
            }
        }
        else
        {
            triageButton.SetActive(true);

            foreach (GameObject item in triageImage)
            {
                item.SetActive(true);
            }
        }

        if (Feedback.hasBandagePatient1 && Feedback.hasOxygenPatient1)
        {
            fullyTreatedPatient1 = true;
        }

        if (Feedback.hasBandagePatient2)
        {
            fullyTreatedPatient2 = true;
        }

        if (Feedback.hasSplintPatient3)
        {
            fullyTreatedPatient3 = true;
        }

        if (Feedback.hasBandagePatient4)
        {
            fullyTreatedPatient4 = true;
        }

        if (Feedback.hasBandagePatient5)
        {
            fullyTreatedPatient5 = true;
        }

        if (Feedback.checkedBreathing1 && Feedback.checkedPulse1 && Feedback.checkedConscious1)
        {
            fullyExaminedPatient1 = true;
        }

        if (Feedback.checkedBreathing2 && Feedback.checkedPulse2 && Feedback.checkedConscious2)
        {
            fullyExaminedPatient2 = true;
        }

        if (Feedback.checkedBreathing3 && Feedback.checkedPulse3 && Feedback.checkedConscious3)
        {
            fullyExaminedPatient3 = true;
        }

        if (Feedback.checkedBreathing4 && Feedback.checkedPulse4 && Feedback.checkedConscious4)
        {
            fullyExaminedPatient4 = true;
        }

        if (Feedback.checkedBreathing5 && Feedback.checkedPulse5 && Feedback.checkedConscious5)
        {
            fullyExaminedPatient5 = true;
        }
    }

    #region FASE_1
    public bool Patient1FullyExamined()
    {
        if (!Feedback.checkedConscious1 || !Feedback.checkedPulse1 || !Feedback.checkedBreathing1 || !Feedback.hasPickAColor1)
        {
            fullyExaminedPatient1 = false;
            return false;
        }
        else
        {
            fullyExaminedPatient1 = true;
            return true;
        }
    }
    public bool Patient2FullyExamined()
    {
        if (!Feedback.checkedConscious2 || !Feedback.checkedPulse2 || !Feedback.checkedBreathing2 || !Feedback.hasPickAColor2)
        {
            fullyExaminedPatient2 = false;
            return false;
        }
        else
        {
            fullyExaminedPatient2 = true;
            return true;
        }
    }
    public bool Patient3FullyExamined()
    {
        if (!Feedback.checkedConscious3 || !Feedback.checkedPulse3 || !Feedback.checkedBreathing3 || !Feedback.hasPickAColor3)
        {
            fullyExaminedPatient3 = false;
            return false;
        }
        else
        {
            fullyExaminedPatient3 = true;
            return true;
        }
    }
    public bool Patient4FullyExamined()
    {
        if (!Feedback.checkedConscious4 || !Feedback.checkedPulse4 || !Feedback.checkedBreathing4 || !Feedback.hasPickAColor4)
        {
            fullyExaminedPatient4 = false;
            return false;
        }
        else
        {
            fullyExaminedPatient4 = true;
            return true;
        }
    }
    public bool Patient5FullyExamined()
    {
        if (!Feedback.checkedConscious5 || !Feedback.checkedPulse5 || !Feedback.checkedBreathing5 || !Feedback.hasPickAColor5)
        {
            fullyExaminedPatient5 = false;
            return false;
        }
        else
        {
            fullyExaminedPatient5 = true;
            return true;
        }
    }
    /*
    public void PhaseScreen()
    {
        phaseScreen.SetActive(true);
        GameObject camera = playerInteract.temporaryCamera;
        playerInteract.MoveCameraBack(camera);
    } 
    */
    #endregion
}
