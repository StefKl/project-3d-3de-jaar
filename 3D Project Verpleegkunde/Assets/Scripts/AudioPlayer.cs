﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    private AudioSource sfx = null;

    // Start is called before the first frame update
    private void Awake()
    {
        sfx = GetComponent<AudioSource>();
    }
    public void PlaySound()
    {
        sfx.Play();
    }
}
