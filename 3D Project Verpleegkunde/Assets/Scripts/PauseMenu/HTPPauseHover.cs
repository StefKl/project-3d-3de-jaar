﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class HTPPauseHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject HTPPauseImage;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        HTPPauseImage.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        HTPPauseImage.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        HTPPauseImage.SetActive(false);
    }
}