﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class ControlsHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject controlsImage;
    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        controlsImage.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        controlsImage.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        controlsImage.SetActive(false);
    }
}