﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class BackButtonHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject backImage;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        backImage.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        backImage.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        backImage.SetActive(false);
    }
}
