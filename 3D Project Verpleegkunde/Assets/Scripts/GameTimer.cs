﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class GameTimer : MonoBehaviour
{
    [SerializeField] private GameObject feedback;
    [SerializeField] private List<Image> patientPhotos;

    [SerializeField] private GameObject briefing;

    [SerializeField] private Sprite patient1;
    [SerializeField] private Sprite patient2;
    [SerializeField] private Sprite patient3;
    [SerializeField] private Sprite patient4;
    [SerializeField] private Sprite patient5;

    [SerializeField] private GameObject[] healthBars;
    [SerializeField] private TMP_Text[] brainInfo;
    [SerializeField] private TMP_Text[] breathingInfo;
    [SerializeField] private TMP_Text[] pulseInfo;
    [SerializeField] private GameObject[] greyHealthBars;
    [SerializeField] private GameObject[] selectedPatient;
    [SerializeField] private GameObject[] notselectedPatient;

    [SerializeField] private GameObject patient1Interact;
    [SerializeField] private GameObject patient2Interact;
    [SerializeField] private GameObject patient3Interact;
    [SerializeField] private GameObject patient4Interact;
    [SerializeField] private GameObject patient5Interact;

    [SerializeField] private Sprite brainRed;
    [SerializeField] private Sprite brainOrange;
    [SerializeField] private Sprite brainGreen;

    [SerializeField] private Sprite lungsRed;
    [SerializeField] private Sprite lungsOrange;
    [SerializeField] private Sprite lungsGreen;

    [SerializeField] private Sprite heartRed;
    [SerializeField] private Sprite heartOrange;
    [SerializeField] private Sprite heartGreen;

    [SerializeField] private Material orange;

    [SerializeField] private Material black;
    [SerializeField] private Material red;
    [SerializeField] private Material yellow;
    [SerializeField] private Material green;
    [SerializeField] private Material blue;
    [SerializeField] private Sprite redTriage;
    [SerializeField] private Sprite blackTriage;
    [SerializeField] private Sprite blueTriage;
    [SerializeField] private Sprite greenTriage;
    [SerializeField] private Sprite yellowTriage;
    [SerializeField] private Sprite orangeTriage;

    [SerializeField] private GameObject triageBar;
    [SerializeField] private float timeTriage;

    [SerializeField] private GameObject consciousButtonPatient1;
    [SerializeField] private GameObject consciousImagePatient1;
    [SerializeField] private GameObject consciousButtonPatient2;
    [SerializeField] private GameObject consciousImagePatient2;
    [SerializeField] private GameObject consciousButtonPatient3;
    [SerializeField] private GameObject consciousImagePatient3;
    [SerializeField] private GameObject consciousButtonPatient4;
    [SerializeField] private GameObject consciousImagePatient4;
    [SerializeField] private GameObject consciousButtonPatient5;
    [SerializeField] private GameObject consciousImagePatient5;

    [SerializeField] private GameObject breathingButtonPatient1;
    [SerializeField] private GameObject breathingImagePatient1;
    [SerializeField] private GameObject breathingButtonPatient2;
    [SerializeField] private GameObject breathingImagePatient2;
    [SerializeField] private GameObject breathingButtonPatient3;
    [SerializeField] private GameObject breathingImagePatient3;
    [SerializeField] private GameObject breathingButtonPatient4;
    [SerializeField] private GameObject breathingImagePatient4;
    [SerializeField] private GameObject breathingButtonPatient5;
    [SerializeField] private GameObject breathingImagePatient5;

    [SerializeField] private GameObject pulseButtonPatient1;
    [SerializeField] private GameObject pulseImagePatient1;
    [SerializeField] private GameObject pulseButtonPatient2;
    [SerializeField] private GameObject pulseImagePatient2;
    [SerializeField] private GameObject pulseButtonPatient3;
    [SerializeField] private GameObject pulseImagePatient3;
    [SerializeField] private GameObject pulseButtonPatient4;
    [SerializeField] private GameObject pulseImagePatient4;
    [SerializeField] private GameObject pulseButtonPatient5;
    [SerializeField] private GameObject pulseImagePatient5;

    private GameObject healthBar1;
    private GameObject healthBar2;
    private GameObject healthBar3;
    private GameObject healthBar4;
    private GameObject healthBar5;

    public static GameObject selectedPatient1;
    public static GameObject selectedPatient2;
    public static GameObject selectedPatient3;
    public static GameObject selectedPatient4;
    public static GameObject selectedPatient5;

    public static GameObject notselectedPatient1;
    public static GameObject notselectedPatient2;
    public static GameObject notselectedPatient3;
    public static GameObject notselectedPatient4;
    public static GameObject notselectedPatient5;
    
    private float timePatient1;
    private float timePatient2;
    private float timePatient3;
    private float timePatient4;
    private float timePatient5;

    private float timeLeftTriage;
    private float timeLeftPatient1;
    private float timeLeftPatient2;
    private float timeLeftPatient3;
    private float timeLeftPatient4;
    private float timeLeftPatient5;

    public float maxTime = 5f;
    private float timeLeft;
    private GameObject[] patients;

    private Color brainImage1Color;

    private bool startCoroutine = true;

    public static List<int> patient1Checks;
    public static List<int> patient2Checks;
    public static List<int> patient3Checks;
    public static List<int> patient4Checks;
    public static List<int> patient5Checks;

    private void Awake()
    {
        startCoroutine = true;
        feedback.SetActive(false);

        patient1Checks = new List<int>();
        patient2Checks = new List<int>();
        patient3Checks = new List<int>();
        patient4Checks = new List<int>();
        patient5Checks = new List<int>();
    }

    // Start is called before the first frame update
    void Start()
    {
        patients = GameObject.FindGameObjectsWithTag("Patient");
        for (int i = 0; i < patients.Length; i++)
        {
            if (patients[i].GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
            {
                patientPhotos[i].sprite = patient1;
                timePatient1 = patients[i].GetComponent<Patient>().patientInfo.healthBarTimer;
                healthBar1 = healthBars[i];
                //brainInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.conscious.ToString();
                //breathingInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.breathing.ToString();
                //pulseInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.pulse.ToString();
                selectedPatient1 = selectedPatient[i];
                notselectedPatient1 = notselectedPatient[i];
                healthBars[i].gameObject.SetActive(true);
                greyHealthBars[i].gameObject.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
            {
                patientPhotos[i].sprite = patient2;
                timePatient2 = patients[i].GetComponent<Patient>().patientInfo.healthBarTimer;
                healthBar2 = healthBars[i];
                //brainInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.conscious.ToString();
                //breathingInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.breathing.ToString();
                //pulseInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.pulse.ToString();
                selectedPatient2 = selectedPatient[i];
                notselectedPatient2 = notselectedPatient[i];
                healthBars[i].gameObject.SetActive(true);
                greyHealthBars[i].gameObject.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
            {
                patientPhotos[i].sprite = patient3;
                timePatient3 = patients[i].GetComponent<Patient>().patientInfo.healthBarTimer;
                healthBar3 = healthBars[i];
                //brainInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.conscious.ToString();
                //breathingInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.breathing.ToString();
                //pulseInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.pulse.ToString();
                selectedPatient3 = selectedPatient[i];
                notselectedPatient3 = notselectedPatient[i];
                healthBars[i].gameObject.SetActive(true);
                greyHealthBars[i].gameObject.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
            {
                patientPhotos[i].sprite = patient4;
                timePatient4 = patients[i].GetComponent<Patient>().patientInfo.healthBarTimer;
                healthBar4 = healthBars[i];
                //brainInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.conscious.ToString();
                //breathingInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.breathing.ToString();
                //pulseInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.pulse.ToString();
                selectedPatient4 = selectedPatient[i];
                notselectedPatient4 = notselectedPatient[i];
                healthBars[i].gameObject.SetActive(true);
                greyHealthBars[i].gameObject.SetActive(true);
            }
            else if (patients[i].GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
            {
                patientPhotos[i].sprite = patient5;
                timePatient5 = patients[i].GetComponent<Patient>().patientInfo.healthBarTimer;
                healthBar5 = healthBars[i];
                //brainInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.conscious.ToString();
                //breathingInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.breathing.ToString();
                //pulseInfo[i].text = patients[i].GetComponent<Patient>().patientInfo.pulse.ToString();
                selectedPatient5 = selectedPatient[i];
                notselectedPatient5 = notselectedPatient[i];
                healthBars[i].gameObject.SetActive(true);
                greyHealthBars[i].gameObject.SetActive(true);
            }
        }

        if (patients.Length == 3)
        {
            //timeTriage = 10;
            timeTriage = 300;
            Debug.Log(timeTriage);
        }
        else if (patients.Length == 4)
        {
            //timeTriage = 20;
            timeTriage = 360;
            Debug.Log(timeTriage);
        }
        else if (patients.Length == 5)
        {
            //timeTriage = 30;
            timeTriage = 420;
            Debug.Log(timeTriage);
        }

        timeLeftTriage = timeTriage;
        timeLeftPatient1 = timePatient1;
        timeLeftPatient2 = timePatient2;
        timeLeftPatient3 = timePatient3;
        timeLeftPatient4 = timePatient4;
        timeLeftPatient5 = timePatient5;

        StartCoroutine(TriagePhase());
    }

    void Update() 
    {
        if (GameFases.examinedAllPatients && startCoroutine && PlayerInteract.startTreatment)
        {
            StartCoroutine(TimerPatient1());
            StartCoroutine(TimerPatient2());
            StartCoroutine(TimerPatient3());
            StartCoroutine(TimerPatient4());
            StartCoroutine(TimerPatient5());
            startCoroutine = false;
        }
    }
    
    private IEnumerator TriagePhase()
    {
        while (timeTriage > 0)
        {
            if (!GameFases.treatedAllPatients)
            {
                if (timeLeftTriage < timeTriage / 2 && timeLeftTriage > timeTriage / 4)
                {
                    triageBar.GetComponent<Image>().color = Color.yellow;
                }
                else if (timeLeftTriage < timeTriage / 4)
                {
                    triageBar.GetComponent<Image>().color = Color.red;
                }
                timeLeftTriage -= Time.deltaTime;
                triageBar.GetComponent<Image>().fillAmount = timeLeftTriage / timeTriage;
                if (GameFases.examinedAllPatients)
                {
                    yield break;
                }
                if (timeLeftTriage <= 0)
                {
                    feedback.SetActive(true);
                }
                yield return null; 
            }
            else
            {
                yield break;
            }
        }
    }
    

    private IEnumerator TimerPatient1()
    {
        while (timeLeftPatient1 > 0)
        {
            if (!GameFases.fullyTreatedPatient1)
            {
                if (timeLeftPatient1 < timePatient1 / 2 && timeLeftPatient1 > timePatient1 / 4)
                {
                    healthBar1.GetComponent<Image>().color = Color.yellow;
                }
                else if (timeLeftPatient1 < timePatient1 / 4)
                {
                    healthBar1.GetComponent<Image>().color = Color.red;
                }
                timeLeftPatient1 -= Time.deltaTime;
                healthBar1.GetComponent<Image>().fillAmount = timeLeftPatient1 / timePatient1;
                if (timeLeftPatient1 <= 0)
                {
                    feedback.SetActive(true);
                }
                yield return null;
            }
            else
            {
                yield break;
            }
        }
    }

    private IEnumerator TimerPatient2()
    {
        while (timeLeftPatient2 > 0)
        {
            if (!GameFases.fullyTreatedPatient2)
            {
                if (timeLeftPatient2 < timePatient2 / 2 && timeLeftPatient2 > timePatient2 / 4)
                {
                    healthBar2.GetComponent<Image>().color = Color.yellow;
                }
                else if (timeLeftPatient2 < timePatient2 / 4)
                {
                    healthBar2.GetComponent<Image>().color = Color.red;
                }
                timeLeftPatient2 -= Time.deltaTime;
                healthBar2.GetComponent<Image>().fillAmount = timeLeftPatient2 / timePatient2;
                if (timeLeftPatient2 <= 0)
                {
                    feedback.SetActive(true);
                }
                yield return null;
            }
            else
            {
                yield break;
            }
        }
    }

    private IEnumerator TimerPatient3()
    {
        while (timeLeftPatient3 > 0)
        {
            if (!GameFases.fullyTreatedPatient3)
            {
                if (timeLeftPatient3 < timePatient3 / 2 && timeLeftPatient3 > timePatient3 / 4)
                {
                    healthBar3.GetComponent<Image>().color = Color.yellow;
                }
                else if (timeLeftPatient3 < timePatient3 / 4)
                {
                    healthBar3.GetComponent<Image>().color = Color.red;
                }
                timeLeftPatient3 -= Time.deltaTime;
                healthBar3.GetComponent<Image>().fillAmount = timeLeftPatient3 / timePatient3;
                if (timeLeftPatient3 <= 0)
                {
                    feedback.SetActive(true);
                }
                yield return null;
            }
            else
            {
                yield break;
            }
        }
    }

    private IEnumerator TimerPatient4()
    {
        while (timeLeftPatient4 > 0)
        {
            if (!GameFases.fullyTreatedPatient4)
            {
                if (timeLeftPatient4 < timePatient4 / 2 && timeLeftPatient4 > timePatient4 / 4)
                {
                    healthBar4.GetComponent<Image>().color = Color.yellow;
                }
                else if (timeLeftPatient4 < timePatient4 / 4)
                {
                    healthBar4.GetComponent<Image>().color = Color.red;
                }
                timeLeftPatient4 -= Time.deltaTime;
                healthBar4.GetComponent<Image>().fillAmount = timeLeftPatient4 / timePatient4;
                if (timeLeftPatient4 <= 0)
                {
                    feedback.SetActive(true);
                }
                yield return null;
            }
            else
            {
                yield break;
            }
        }
    }

    private IEnumerator TimerPatient5()
    {
        while (timeLeftPatient5 > 0)
        {
            if (!GameFases.fullyTreatedPatient5)
            {
                if (timeLeftPatient5 < timePatient5 / 2 && timeLeftPatient5 > timePatient5 / 4)
                {
                    healthBar5.GetComponent<Image>().color = Color.yellow;
                }
                else if (timeLeftPatient5 < timePatient5 / 4)
                {
                    healthBar5.GetComponent<Image>().color = Color.red;
                }
                timeLeftPatient5 -= Time.deltaTime;
                healthBar5.GetComponent<Image>().fillAmount = timeLeftPatient5 / timePatient5;
                if (timeLeftPatient5 <= 0)
                {
                    feedback.SetActive(true);
                }
                yield return null;
            }
            else
            {
                yield break;
            }
        }
    }

    public void ConsciousButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            //Feedback.checkedConscious1 = true;
            IconHover.checkedConsciousness1 = true;
            if (Feedback.checkedConscious1 == true)
            {
                if (PlayerInteract.patient.patientInfo.conscious == Conscious.Alert)
                {
                    healthBar1.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainGreen;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.PainResponse)
                {
                    healthBar1.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.Unresponsive)
                {
                    healthBar1.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.VerbalResponse)
                {
                    healthBar1.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainOrange;
                }

                consciousImagePatient1.SetActive(true);
                consciousButtonPatient1.SetActive(false);
                patient1Checks.Add(1);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            //Feedback.checkedConscious2 = true;
            IconHover.checkedConsciousness2 = true;
            if (Feedback.checkedConscious2 == true)
            {
                if (PlayerInteract.patient.patientInfo.conscious == Conscious.Alert)
                {
                    healthBar2.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainGreen;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.PainResponse)
                {
                    healthBar2.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.Unresponsive)
                {
                    healthBar2.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.VerbalResponse)
                {
                    healthBar2.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainOrange;
                }

                consciousImagePatient2.SetActive(true);
                consciousButtonPatient2.SetActive(false);
                patient2Checks.Add(1);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            //Feedback.checkedConscious3 = true;
            IconHover.checkedConsciousness3 = true;
            if (Feedback.checkedConscious3 == true)
            {
                if (PlayerInteract.patient.patientInfo.conscious == Conscious.Alert)
                {
                    healthBar3.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainGreen;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.PainResponse)
                {
                    healthBar3.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.Unresponsive)
                {
                    healthBar3.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.VerbalResponse)
                {
                    healthBar3.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainOrange;
                }

                consciousImagePatient3.SetActive(true);
                consciousButtonPatient3.SetActive(false);
                patient3Checks.Add(1);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            //Feedback.checkedConscious4 = true;
            IconHover.checkedConsciousness4 = true;
            if (Feedback.checkedConscious4 == true)
            {
                if (PlayerInteract.patient.patientInfo.conscious == Conscious.Alert)
                {
                    healthBar4.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainGreen;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.PainResponse)
                {
                    healthBar4.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.Unresponsive)
                {
                    healthBar4.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.VerbalResponse)
                {
                    healthBar4.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainOrange;
                }

                consciousImagePatient4.SetActive(true);
                consciousButtonPatient4.SetActive(false);
                patient4Checks.Add(1);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            //Feedback.checkedConscious5 = true;
            IconHover.checkedConsciousness5 = true;
            if (Feedback.checkedConscious5 == true)
            {
                if (PlayerInteract.patient.patientInfo.conscious == Conscious.Alert)
                {
                    healthBar5.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainGreen;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.PainResponse)
                {
                    healthBar5.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.Unresponsive)
                {
                    healthBar5.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainRed;
                }
                else if (PlayerInteract.patient.patientInfo.conscious == Conscious.VerbalResponse)
                {
                    healthBar5.transform.Find("BrainImage1").GetComponent<Image>().sprite = brainOrange;
                }

                consciousImagePatient5.SetActive(true);
                consciousButtonPatient5.SetActive(false);
                patient5Checks.Add(1);
            }

        }
    }

    public void PulseButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            //Feedback.checkedPulse1 = true;
            IconHover.checkedPulse1 = true;
            if (Feedback.checkedPulse1 == true)
            {
                if (PlayerInteract.patient.patientInfo.pulse == Pulse.Fast)
                {
                    healthBar1.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Regular)
                {
                    healthBar1.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartGreen;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Slow)
                {
                    healthBar1.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.None)
                {
                    healthBar1.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartRed;
                }

                pulseImagePatient1.SetActive(true);
                pulseButtonPatient1.SetActive(false);
                patient1Checks.Add(2);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            //Feedback.checkedPulse2 = true;
            IconHover.checkedPulse2 = true;
            if (Feedback.checkedPulse2 == true)
            {
                if (PlayerInteract.patient.patientInfo.pulse == Pulse.Fast)
                {
                    healthBar2.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Regular)
                {
                    healthBar2.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartGreen;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Slow)
                {
                    healthBar2.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.None)
                {
                    healthBar2.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartRed;
                }

                pulseImagePatient2.SetActive(true);
                pulseButtonPatient2.SetActive(false);
                patient2Checks.Add(2);
            }  
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            //Feedback.checkedPulse3 = true;
            IconHover.checkedPulse3 = true;
            if (Feedback.checkedPulse3 == true)
            {        
                if (PlayerInteract.patient.patientInfo.pulse == Pulse.Fast)
                {
                    healthBar3.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Regular)
                {
                   healthBar3.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartGreen;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Slow)
                {
                    healthBar3.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.None)
                {
                    healthBar3.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartRed;
                }

                pulseImagePatient3.SetActive(true);
                pulseButtonPatient3.SetActive(false);
                patient3Checks.Add(2);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            //Feedback.checkedPulse4 = true;
            IconHover.checkedPulse4 = true;
            if (Feedback.checkedPulse4 == true)
            {
                if (PlayerInteract.patient.patientInfo.pulse == Pulse.Fast)
                {
                    healthBar4.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Regular)
                {
                    healthBar4.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartGreen;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Slow)
                {
                    healthBar4.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.None)
                {
                    healthBar4.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartRed;
                }

                pulseImagePatient4.SetActive(true);
                pulseButtonPatient4.SetActive(false);
                patient4Checks.Add(2);
            }

        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            //Feedback.checkedPulse5 = true;
            IconHover.checkedPulse5 = true;
            if (Feedback.checkedPulse5 == true)
            {
                if (PlayerInteract.patient.patientInfo.pulse == Pulse.Fast)
                {
                    healthBar5.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Regular)
                {
                    healthBar5.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartGreen;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.Slow)
                {
                    healthBar5.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartOrange;
                }
                else if (PlayerInteract.patient.patientInfo.pulse == Pulse.None)
                {
                    healthBar5.transform.Find("HeartImage1").GetComponent<Image>().sprite = heartRed;
                }

                pulseImagePatient5.SetActive(true);
                pulseButtonPatient5.SetActive(false);
                patient5Checks.Add(2);
            }

        }
    }

    public void BreathingButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            IconHover.checkedBreathing1 = true;
            if (Feedback.checkedBreathing1)
            {
                if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Deep)
                {
                    healthBar1.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsGreen;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Fast)
                {
                    healthBar1.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Shallow)
                {
                    healthBar1.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Slow)
                {
                    healthBar1.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Not)
                {
                    healthBar1.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }

                breathingImagePatient1.SetActive(true);
                breathingButtonPatient1.SetActive(false);
                patient1Checks.Add(3);
            }
            
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            IconHover.checkedBreathing2 = true;
            if (Feedback.checkedBreathing2)
            {
                if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Deep)
                {
                    healthBar2.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsGreen;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Fast)
                {
                    healthBar2.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Shallow)
                {
                    healthBar2.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Slow)
                {
                    healthBar2.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Not)
                {
                    healthBar2.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }

                breathingImagePatient2.SetActive(true);
                breathingButtonPatient2.SetActive(false);
                patient2Checks.Add(3);
            }
            
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            IconHover.checkedBreathing3 = true;
            if (Feedback.checkedBreathing3)
            {
                if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Deep)
                {
                    healthBar3.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsGreen;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Fast)
                {
                    healthBar3.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Shallow)
                {
                    healthBar3.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Slow)
                {
                    healthBar3.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Not)
                {
                    healthBar3.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }

                breathingImagePatient3.SetActive(true);
                breathingButtonPatient3.SetActive(false);
                patient3Checks.Add(3);
            }
            
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            IconHover.checkedBreathing4 = true;
            if (Feedback.checkedBreathing4)
            {
                if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Deep)
                {
                    healthBar4.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsGreen;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Fast)
                {
                    healthBar4.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Shallow)
                {
                    healthBar4.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Slow)
                {
                    healthBar4.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Not)
                {
                    healthBar4.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }

                breathingImagePatient4.SetActive(true);
                breathingButtonPatient4.SetActive(false);
                patient4Checks.Add(3);
            }
            
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            IconHover.checkedBreathing5 = true;
            if (Feedback.checkedBreathing5)
            {
                if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Deep)
                {
                    healthBar5.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsGreen;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Fast)
                {
                    healthBar5.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Shallow)
                {
                    healthBar5.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Slow)
                {
                    healthBar5.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsOrange;
                }
                else if (PlayerInteract.patient.patientInfo.breathing == BreathingPatern.Not)
                {
                    healthBar5.transform.Find("LungsImage1").GetComponent<Image>().sprite = lungsRed;
                }

                breathingImagePatient5.SetActive(true);
                breathingButtonPatient5.SetActive(false);
                patient5Checks.Add(3);
            }
            
        }
    }

    #region Radial Buttons Color

    public void RedButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            healthBar1.transform.Find("TriageColor").GetComponent<Image>().sprite = redTriage;
            Feedback.hasPickAColor1 = true;
            selectedPatient1.GetComponent<Image>().color = red.color;
            notselectedPatient1.GetComponent<Image>().color = red.color;
            Feedback.pickedColor1 = red.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            healthBar2.transform.Find("TriageColor").GetComponent<Image>().sprite = redTriage;
            Feedback.hasPickAColor2 = true;
            selectedPatient2.GetComponent<Image>().color = red.color;
            notselectedPatient2.GetComponent<Image>().color = red.color;
            Feedback.pickedColor2 = red.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            healthBar3.transform.Find("TriageColor").GetComponent<Image>().sprite = redTriage;
            Feedback.hasPickAColor3 = true;
            selectedPatient3.GetComponent<Image>().color = red.color;
            notselectedPatient3.GetComponent<Image>().color = red.color;
            Feedback.pickedColor3 = red.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            healthBar4.transform.Find("TriageColor").GetComponent<Image>().sprite = redTriage;
            Feedback.hasPickAColor4 = true;
            notselectedPatient4.GetComponent<Image>().color = red.color;
            selectedPatient4.GetComponent<Image>().color = red.color;
            Feedback.pickedColor4 = red.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            healthBar5.transform.Find("TriageColor").GetComponent<Image>().sprite = redTriage;
            Feedback.hasPickAColor5 = true;
            selectedPatient5.GetComponent<Image>().color = red.color;
            notselectedPatient5.GetComponent<Image>().color = red.color;
            Feedback.pickedColor5 = red.color;
        }
        PatientInteraction.hasClickedOnButton = true;
    }

    public void BlackButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            healthBar1.transform.Find("TriageColor").GetComponent<Image>().sprite = blackTriage;
            Feedback.hasPickAColor1 = true;
            selectedPatient1.GetComponent<Image>().color = black.color;
            notselectedPatient1.GetComponent<Image>().color = black.color;
            Feedback.pickedColor1 = black.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            healthBar2.transform.Find("TriageColor").GetComponent<Image>().sprite = blackTriage;
            Feedback.hasPickAColor2 = true;
            selectedPatient2.GetComponent<Image>().color = black.color;
            notselectedPatient2.GetComponent<Image>().color = black.color;
            Feedback.pickedColor2 = black.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            healthBar3.transform.Find("TriageColor").GetComponent<Image>().sprite = blackTriage;
            Feedback.hasPickAColor3 = true;
            selectedPatient3.GetComponent<Image>().color = black.color;
            notselectedPatient3.GetComponent<Image>().color = black.color;
            Feedback.pickedColor3 = black.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            healthBar4.transform.Find("TriageColor").GetComponent<Image>().sprite = blackTriage;
            Feedback.hasPickAColor4 = true;
            selectedPatient4.GetComponent<Image>().color = black.color;
            notselectedPatient4.GetComponent<Image>().color = black.color;
            Feedback.pickedColor4 = black.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            healthBar5.transform.Find("TriageColor").GetComponent<Image>().sprite = blackTriage;
            Feedback.hasPickAColor5 = true;
            selectedPatient5.GetComponent<Image>().color = black.color;
            notselectedPatient5.GetComponent<Image>().color = black.color;
            Feedback.pickedColor5 = black.color;
        }
        PatientInteraction.hasClickedOnButton = true;
    }

    public void OrangeButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            healthBar1.transform.Find("TriageColor").GetComponent<Image>().sprite = orangeTriage;
            Feedback.hasPickAColor1 = true;
            selectedPatient1.GetComponent<Image>().color = orange.color;
            notselectedPatient1.GetComponent<Image>().color = orange.color;
            Feedback.pickedColor1 = orange.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            healthBar2.transform.Find("TriageColor").GetComponent<Image>().sprite = orangeTriage;
            Feedback.hasPickAColor2 = true;
            selectedPatient2.GetComponent<Image>().color = orange.color;
            notselectedPatient2.GetComponent<Image>().color = orange.color;
            Feedback.pickedColor2 = orange.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            healthBar3.transform.Find("TriageColor").GetComponent<Image>().sprite = orangeTriage;
            Feedback.hasPickAColor3 = true;
            selectedPatient3.GetComponent<Image>().color = orange.color;
            notselectedPatient3.GetComponent<Image>().color = orange.color;
            Feedback.pickedColor3 = orange.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            healthBar4.transform.Find("TriageColor").GetComponent<Image>().sprite = orangeTriage;
            Feedback.hasPickAColor4 = true;
            selectedPatient4.GetComponent<Image>().color = orange.color;
            notselectedPatient4.GetComponent<Image>().color = orange.color;
            Feedback.pickedColor4 = orange.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            healthBar5.transform.Find("TriageColor").GetComponent<Image>().sprite = orangeTriage;
            Feedback.hasPickAColor5 = true;
            selectedPatient5.GetComponent<Image>().color = orange.color;
            notselectedPatient5.GetComponent<Image>().color = orange.color;
            Feedback.pickedColor5 = orange.color;
        }
        PatientInteraction.hasClickedOnButton = true;
    }

    public void YellowButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            healthBar1.transform.Find("TriageColor").GetComponent<Image>().sprite = yellowTriage;
            Feedback.hasPickAColor1 = true;
            selectedPatient1.GetComponent<Image>().color = yellow.color;
            notselectedPatient1.GetComponent<Image>().color = yellow.color;
            Feedback.pickedColor1 = yellow.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            healthBar2.transform.Find("TriageColor").GetComponent<Image>().sprite = yellowTriage;
            Feedback.hasPickAColor2 = true;
            selectedPatient2.GetComponent<Image>().color = yellow.color;
            notselectedPatient2.GetComponent<Image>().color = yellow.color;
            Feedback.pickedColor2 = yellow.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            healthBar3.transform.Find("TriageColor").GetComponent<Image>().sprite = yellowTriage;
            Feedback.hasPickAColor3 = true;
            selectedPatient3.GetComponent<Image>().color = yellow.color;
            notselectedPatient3.GetComponent<Image>().color = yellow.color;
            Feedback.pickedColor3 = yellow.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            healthBar4.transform.Find("TriageColor").GetComponent<Image>().sprite = yellowTriage;
            Feedback.hasPickAColor4 = true;
            selectedPatient4.GetComponent<Image>().color = yellow.color;
            notselectedPatient4.GetComponent<Image>().color = yellow.color;
            Feedback.pickedColor4 = yellow.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            healthBar5.transform.Find("TriageColor").GetComponent<Image>().sprite = yellowTriage;
            Feedback.hasPickAColor5 = true;
            selectedPatient5.GetComponent<Image>().color = yellow.color;
            notselectedPatient5.GetComponent<Image>().color = yellow.color;
            Feedback.pickedColor5 = yellow.color;
        }
        PatientInteraction.hasClickedOnButton = true;
    }

    public void GreenButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            healthBar1.transform.Find("TriageColor").GetComponent<Image>().sprite = greenTriage;
            Feedback.hasPickAColor1 = true;
            selectedPatient1.GetComponent<Image>().color = green.color;
            notselectedPatient1.GetComponent<Image>().color = green.color;
            Feedback.pickedColor1 = green.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            healthBar2.transform.Find("TriageColor").GetComponent<Image>().sprite = greenTriage;
            Feedback.hasPickAColor2 = true;
            selectedPatient2.GetComponent<Image>().color = green.color;
            notselectedPatient2.GetComponent<Image>().color = green.color;
            Feedback.pickedColor2 = green.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            healthBar3.transform.Find("TriageColor").GetComponent<Image>().sprite = greenTriage;
            Feedback.hasPickAColor3 = true;
            selectedPatient3.GetComponent<Image>().color = green.color;
            notselectedPatient3.GetComponent<Image>().color = green.color;
            Feedback.pickedColor3 = green.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            healthBar4.transform.Find("TriageColor").GetComponent<Image>().sprite = greenTriage;
            Feedback.hasPickAColor4 = true;
            selectedPatient4.GetComponent<Image>().color = green.color;
            notselectedPatient4.GetComponent<Image>().color = green.color;
            Feedback.pickedColor4 = green.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            healthBar5.transform.Find("TriageColor").GetComponent<Image>().sprite = greenTriage;
            Feedback.hasPickAColor5 = true;
            selectedPatient5.GetComponent<Image>().color = green.color;
            notselectedPatient5.GetComponent<Image>().color = green.color;
            Feedback.pickedColor5 = green.color;
        }
        PatientInteraction.hasClickedOnButton = true;
    }

    public void BlueButton()
    {
        if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient1InjuryUI"))
        {
            healthBar1.transform.Find("TriageColor").GetComponent<Image>().sprite = blueTriage;
            Feedback.hasPickAColor1 = true;
            selectedPatient1.GetComponent<Image>().color = blue.color;
            notselectedPatient1.GetComponent<Image>().color = blue.color;
            Feedback.pickedColor1 = blue.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient2InjuryUI"))
        {
            healthBar2.transform.Find("TriageColor").GetComponent<Image>().sprite = blueTriage;
            Feedback.hasPickAColor2 = true;
            selectedPatient2.GetComponent<Image>().color = blue.color;
            notselectedPatient2.GetComponent<Image>().color = blue.color;
            Feedback.pickedColor2 = blue.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient3InjuryUI"))
        {
            healthBar3.transform.Find("TriageColor").GetComponent<Image>().sprite = blueTriage;
            Feedback.hasPickAColor3 = true;
            selectedPatient3.GetComponent<Image>().color = blue.color;
            notselectedPatient3.GetComponent<Image>().color = blue.color;
            Feedback.pickedColor3 = blue.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient4InjuryUI"))
        {
            healthBar4.transform.Find("TriageColor").GetComponent<Image>().sprite = blueTriage;
            Feedback.hasPickAColor4 = true;
            selectedPatient4.GetComponent<Image>().color = blue.color;
            notselectedPatient4.GetComponent<Image>().color = blue.color;
            Feedback.pickedColor4 = blue.color;
        }
        else if (PlayerInteract.patient.GetComponent<CustomTag>().HasTag("Patient5InjuryUI"))
        {
            healthBar5.transform.Find("TriageColor").GetComponent<Image>().sprite = blueTriage;
            Feedback.hasPickAColor5 = true;
            selectedPatient5.GetComponent<Image>().color = blue.color;
            notselectedPatient5.GetComponent<Image>().color = blue.color;
            Feedback.pickedColor5 = blue.color;
        }
        PatientInteraction.hasClickedOnButton = true;
    }
    #endregion
}
