﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class QuitHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject quitImage;

    public TextMeshProUGUI quitText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        quitImage.SetActive(true);
        quitText.color = Color.white;

    }
    public void OnPointerExit(PointerEventData evenData)
    {
        quitImage.SetActive(false);
        quitText.color = Color.grey;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        quitImage.SetActive(false);
        quitText.color = Color.grey;

    }
}