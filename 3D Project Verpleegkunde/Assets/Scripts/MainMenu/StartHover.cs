﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class StartHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject startImage;
    private Color temporaryColor;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        temporaryColor = this.GetComponent<Image>().color;
        temporaryColor.a = 0f;
        this.GetComponent<Image>().color = temporaryColor;

        startImage.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        temporaryColor = this.GetComponent<Image>().color;
        temporaryColor.a = 1f;
        this.GetComponent<Image>().color = temporaryColor;

        startImage.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        temporaryColor = this.GetComponent<Image>().color;
        temporaryColor.a = 1f;
        this.GetComponent<Image>().color = temporaryColor;

        startImage.SetActive(false);
    }
}
