﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class HTPHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private GameObject howToPlayImage;

    public TextMeshProUGUI HTPText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("Hovered");
        howToPlayImage.SetActive(true);
        HTPText.color = Color.white;

    }
    public void OnPointerExit(PointerEventData evenData)
    {
        howToPlayImage.SetActive(false);
        HTPText.color = Color.grey;

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        howToPlayImage.SetActive(false);
        HTPText.color = Color.grey;

    }
}
