﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour, IPointerEnterHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {

    }
    void Start()
    {
        Cursor.visible = true;
        Screen.lockCursor = false;
    }
    public void StartButton()
    {
        SceneManager.LoadScene("Cutscene");
    }
    public void PlayGameTunnel()
    {
        //SceneManager.LoadScene("Tunnel");
    }
    public void QuitGame()
    {
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }

}
